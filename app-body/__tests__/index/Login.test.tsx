import React from "react";
import { render, screen, cleanup , fireEvent, RenderResult} from '../utils';
import Login from "../../components/login/Login";
import { findByText } from "@testing-library/react";

describe("로그인 페이지 테스트", ()=>{
    let container : RenderResult;

    // 1. 렌더링 테스트
    beforeEach(()=>{
        container=render(<Login></Login>);
    })

    afterEach(cleanup);

    // 2. 요소 확인 테스트
    it('로그인 페이지 렌더링 테스트' , async()=>{
       expect(screen.getByText("GEC 스마트팩토리 모니터링 솔루션")).toBeInTheDocument;
    })

    // 3. 이벤트 발생 후 화면 변화 테스트
    it('로그인 입력 테스트', async()=>{
        const idInput :any= container.getByPlaceholderText("아이디");
        const passwordInput :any = container.getByPlaceholderText("패스워드");

        expect(idInput.value).toBe('');
        fireEvent.change(idInput, {target :{value :'admin'}});
        expect(idInput.value).toBe('admin');

        expect(passwordInput.value).toBe("");
        fireEvent.change(passwordInput, { target: { value: "admin123!" } });
        expect(passwordInput.value).toBe("admin123!");
    })

    it('로그인 입력 실패시', async()=>{
        const button = screen.getByText("로그인");
        
        fireEvent.click(button);

        // expect(
        //   screen.getAllByText("가입하지 않은 아이디거나, 잘못된 비밀번호 입니다")
        // ).toBeInTheDocument();
    });
}) 