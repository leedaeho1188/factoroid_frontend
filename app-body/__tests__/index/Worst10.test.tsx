import React from "react";
import {cleanup} from "../utils";
import {api} from "../../api/api";
import {Worst10API} from '../../api/api';
import {worst10MockData} from '../../__mocks__/dataMock';

describe("Worst10 페이지 테스트", ()=>{
    let spyGet:any;
    let Worst10SpyGet:any;

    beforeEach(async()=>{
        // 1. API 호출 함수 모킹
        api.get = jest.fn().mockResolvedValue({
          data: worst10MockData,
        });
        
        spyGet = jest.spyOn(api, "get");
        Worst10SpyGet = jest.spyOn(Worst10API, "yulkok"); 
    })

    afterEach(cleanup);

    it('모킹 API 테스트',async()=>{
        // 2. 비동기 함수 호출 
        const result = await Worst10API.yulkok("2022-02-02", "2022-02-02", "all");

        // 3. 호출 REST API 엔드 포인트 확인
        expect(spyGet).toBeCalledTimes(1);
        expect(spyGet).toBeCalledWith(
          "api/etc/worst10/8?beginDateTime=2022-02-02&endDateTime=2022-02-02&timeType=all"
        );
    })

})
 
