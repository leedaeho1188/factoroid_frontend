import worst10, {initializeData, worst10Thunk} from '../../store/modules/worst10';
import { worst10MockData } from "../../__mocks__/dataMock";
import {api} from "../../api/api";
import store from "../../store/store";
import CardListContainer from "../../components/factory/worst10/CardListContainer";
import {
  render,
  screen,
} from "../utils";


describe("worst10 리듀서 테스트", ()=>{
    // API 호출 함수 모킹 
    beforeEach(async()=>{
        api.get=jest.fn().mockResolvedValue({
            data: worst10MockData,
        })
    });

    // 1. 초기 상태 테스트
    it("초기 상태 확인", ()=>{
        expect(worst10(undefined, {type: '@@INIT'})).toEqual({data:[{}], status:""})
    })

    // 2. 리듀서 테스트
    it('초기화 액션 확인',()=>{
        let state= worst10(undefined, initializeData(worst10MockData.results));
        expect(state.data).toEqual(worst10MockData.results);
    })

    // 2-A. Thunk 리듀서 테스트
    it('비동기 데이터 패칭 Thunk 확인',async()=>{
        
        await store.dispatch(worst10Thunk({
            startDate:"2022-02-02",
            endDate:"2022-02-02",
            workingHour: "all"
        }))
        let state=store.getState().worst10;
        expect(state.data).toEqual(worst10MockData.results);
        expect(state.status).toEqual("success");
    })

    // 3. 컨테이너 컴포넌트 테스트
    it("API를 호출해서, 카드 컴포넌트를 렌더링 해야 한다.", async () => {
      await render(<CardListContainer></CardListContainer>);
      expect(screen.getByText("가동률 Worst 10")).toBeInTheDocument;
      expect(screen.getByText("최초 동작 Worst10")).toBeInTheDocument;
    });
})