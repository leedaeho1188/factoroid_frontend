import selected, {clickLoss, clickIdle, clickOff} from "../../store/modules/selected";

describe("selected 리듀서 테스트", ()=>{
    // 1. 초기 상태 확인 
    it("초기 상태 확인", ()=>{
          expect(selected(undefined, { type: "@@INIT" })).toEqual({
            data: {
              loss: true,
              idle: false,
              off: false,
            },
            selected: "loss",
          });
    })

    // 2. 리듀서 테스트
    it("clickLoss 액션 확인",()=>{
        let state=selected(undefined, clickLoss());
        expect(state.data.loss).toEqual(true);
        expect(state.data.idle).toEqual(false);
        expect(state.data.off).toEqual(false);
        expect(state.selected).toEqual("loss");
    });

    // 2. 리듀서 테스트 
    it("clickIdle 액션 확인", () => {
      let state = selected(undefined, clickIdle());
      expect(state.data.idle).toEqual(true);
      expect(state.data.loss).toEqual(false);
      expect(state.data.off).toEqual(false);
      expect(state.selected).toEqual("idle");
    });

    // 2. 리듀서 테스트
    it("clickLoss 액션 확인", () => {
      let state = selected(undefined, clickOff());
      expect(state.data.off).toEqual(true);
      expect(state.data.loss).toEqual(false);
      expect(state.data.idle).toEqual(false);
      expect(state.selected).toEqual("off");
    });
})