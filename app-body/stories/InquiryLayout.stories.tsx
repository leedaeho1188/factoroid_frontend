import React from "react";
import { ComponentStory, ComponentMeta } from '@storybook/react';

import InquiryLayout from "../components/commons/InquiryLayout/InquiryLayout";

export default {
    title: "common/InquiryLayout",
    component: InquiryLayout,
} as ComponentMeta<typeof InquiryLayout>;


const Template: ComponentStory<typeof InquiryLayout> = (args) => (
    <InquiryLayout {...args} />
)

export const Base = Template.bind({});
Base.args = {
    timePicker:true,
    workingHour:true,
    equipmentType:true,
    date:{
        startDate:new Date(),
        endDate:new Date(),
    }
}