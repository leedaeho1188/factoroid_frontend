import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import  StickChart  from '../components/charts/StickChart';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Chart/StickChart",
  component: StickChart,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as ComponentMeta<typeof StickChart>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof StickChart> = (args) => (
  <StickChart {...args} />
);

export const Base = Template.bind({});
Base.args = {
  data:[10,20,30],
  color:['pink','yellow','green']
};
