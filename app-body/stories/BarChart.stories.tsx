import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import BarChart from "../components/charts/BarChart";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Chart/BarChart",
  component: BarChart,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as ComponentMeta<typeof BarChart>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof BarChart> = (args) => (
  <BarChart />
);

export const Base = Template.bind({});
Base.args = {
};
