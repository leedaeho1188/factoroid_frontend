import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { MultipleCalendars } from '../components/commons/Calendar/CalendarLayout';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "common/Calendar",
  component: MultipleCalendars,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as ComponentMeta<typeof MultipleCalendars>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof MultipleCalendars> = (args) => (
  <MultipleCalendars {...args} />
);

export const Base = Template.bind({});
Base.args = {
  date: new Date(),
  start: new Date(),
  end: new Date(new Date().getFullYear(), new Date().getMonth()+1, new Date().getDate()),
};