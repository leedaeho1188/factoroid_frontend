import React from "react";

import { ComponentStory, ComponentMeta } from "@storybook/react";
import depthInfo from "../shared/depthInfo.json";

const data= depthInfo;

import DepthSelectModal from "../components/commons/DepthSelectModal/DepthSelectModal";

export default {
  title: "Common/DepthSelectModal",
  component: DepthSelectModal,
} as ComponentMeta<typeof DepthSelectModal>;


const Template: ComponentStory<typeof DepthSelectModal> = (args) => (
  <DepthSelectModal {...args} />
);

export const Base = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Base.args = {
  id:8,
  factory: "율곡",
  data: data,
};
