import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Button from "../components/commons/elements/buttons/Button";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Common/Button",
  component: Button,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as ComponentMeta<typeof Button>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;


export const PrimarySolid = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
PrimarySolid.args = {
  size: "large",
  color: "primary",
  text: "Button",
  style: "solid",
};

export const SecondarySolid = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
SecondarySolid.args = {
  size: "large",
  color: "secondary",
  text: "Button",
  style: "solid",
};

export const GraySolid = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
GraySolid.args = {
  size: "large",
  color: "gray",
  text: "Button",
  style: "solid",
};


export const PrimaryLine = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
PrimaryLine.args = {
  size: "large",
  color: "primary",
  text: "Button",
  style: "line",
};

export const SecondaryLine = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
SecondaryLine.args = {
  size: "large",
  color: "secondary",
  text: "Button",
  style: "line",
};

export const GrayLine = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
GrayLine.args = {
  size: "large",
  color: "gray",
  text: "Button",
  style: "line",
};

export const PrimaryText = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
PrimaryText.args = {
  size: "large",
  color: "primary",
  text: "Button",
  style: "text",
};

export const SecondaryText = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
SecondaryText.args = {
  size: "large",
  color: "secondary",
  text: "Button",
  style: "text",
};

export const GrayText = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
GrayText.args = {
  size: "large",
  color: "gray",
  text: "Button",
  style: "text",
}