import React from 'react'
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Input from '../components/commons/elements/Input'

export default {
    title: "Common/Input",
    component: Input,   
} as ComponentMeta<typeof Input>;
