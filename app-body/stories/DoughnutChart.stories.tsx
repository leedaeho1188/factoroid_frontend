import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import DoughnutChart from "../components/charts/DoughnutChart";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Chart/DoughnutChart",
  component: DoughnutChart,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as ComponentMeta<typeof DoughnutChart>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof DoughnutChart> = (args) => (
  <DoughnutChart />
);

export const Base = Template.bind({});
Base.args = {};
