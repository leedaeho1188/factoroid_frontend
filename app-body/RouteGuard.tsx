import { useEffect,useState} from "react";
import {useRouter} from 'next/router';
import {useDispatch } from "react-redux";
import { CurrentLoginThunk } from "./store/modules/login";
import { customersInfo } from "shared/customerInfo";

export {RouteGuard};


// DESCRIBE : URL 직접 쳐서 접근하는 것을 막는 모듈.
function RouteGuard({children}:any){
    const dispatch = useDispatch();
    const router=useRouter();
    // DESCRIBE : 로그인 유무 상태관리
    const [isAuthenticated, setIsAuthenticated] = useState<boolean>(true);

    useEffect(() => {
        console.log("Route Guards");
        // DESCRIBE : RouteGuard 초기 렌더링시에 authCheck함수 실행
        authCheck(router.asPath);

        // DESCRIBE : route변경 시작시에 authCheck함수 실행 
        router.events.on('routeChangeStart', (nextPath) => (authCheck(nextPath)))  
    }, []);

    const authCheck = async(nextPath:string) => {
        // DESCRIBE : 로그인한 상태인지 확인
        const result:any =  await dispatch(CurrentLoginThunk());
        const loginStatus = result.payload.status;
        const data = result.payload.result?.data;
        if(loginStatus === "success"){
          setIsAuthenticated(true);
        }else{
          setIsAuthenticated(false);
        }

        // DESCRIBE : 로그인안한 상태일 때
        if (loginStatus !== "success") {
          // DESCRIBE : login페이지로 이동
          if(nextPath !== "/login/"){
            router.replace('/login');
          }
        }
        // DESCRIBE : 로그인한 상태일 때 
        // TODO: admin으로 로그인시에 어떻게 적용해야할지.. 추가 필요
        else {
          const index = customersInfo.findIndex((e) => e.customer.title === data.customer.name);

          // DESCRIBE : 고객사 페이지가 아닌 다른 페이지로 이동할 때 고객사 페이지로 redirect
          if(nextPath.includes('/login/') || !nextPath.includes(`${customersInfo[index].customer.path}`)){
            router.replace(`${customersInfo[index].customer.path}/${customersInfo[index].pages[0].path}`);
          }
      }
    }

    // DESCRIBE : 로그인 안했는데 로그인 페이지가 아닐경우 || 로그인 했는데 로그인 페이지일 경우  => 페이지 component 렌더링 안하기
    if((!isAuthenticated && router.asPath !== "/login/") || (isAuthenticated && router.asPath === "/login/")){
      return null;
    }

    return (children);
}

