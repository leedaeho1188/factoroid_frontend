import {
  Worst10Props,
} from "../../../types/yulkok/worst10-types";
import Worst10 from "../../../components/factory/worst10/Worst10";
import { Worst10API } from "../../../api/api";


/**
 * 
 * @param equipments : 장비리스트
 * @param today_date : 오늘 시간,
 * @param seconds : 초 (테스트용, 삭제 예정)
 * @param minutes : 분 (테스트용, 삭제 예정)
 * @returns 
 */
function Worst10Page({equipments}: Worst10Props) {

  return <Worst10 equipments={equipments}></Worst10>;
}

export async function getStaticProps() {

  // const today=new Date();
  // const year= today.getFullYear();
  // const month=today.getMonth()+1<10?`0${today.getMonth()+1}`:today.getMonth()+1;
  // const day = today.getDate()-1 < 10 ? `0${today.getDate()-1}` : today.getDate()-1;

  // const today_date=`${year}-${month}-${day}`;
  const seconds = new Date().getSeconds();
  const minutes = new Date().getMinutes();
  const now_date= `${minutes}분 ${seconds}초`
  const today_date="2022-03-02";
  
  const result = await Worst10API
                      .yulkok(today_date, today_date, "all")
                      .then((res)=> {return res})
                      .catch((err)=>{console.log('worst10 데이터 패칭 에러', err); return err})

  
  if(result==null){
    const equipments:Array<any>=[];
    return {props: {equipments}};
  }
  
  const equipments = result.results;

  return {
    props : {
      equipments,
    },
    revalidate:5,
  };
}
export default Worst10Page;
