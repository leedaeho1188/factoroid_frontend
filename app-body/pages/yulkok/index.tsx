import React, { useEffect } from "react";
import { useRouter } from "next/router";

const Yulkok = () => {
    const router = useRouter()

    useEffect(()=>{
        router.push(router.asPath+'/home')
    },[])

    return(
        <></>
    )
}


export default Yulkok;