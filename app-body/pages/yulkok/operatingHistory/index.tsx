import React, {useEffect, useState, useRef} from 'react';
import Head from 'next/head';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { dehydrate, QueryClient, useQuery } from 'react-query';

import { RootState } from 'store/modules';
import Layout from 'components/commons/EntireLayout/Layout';
import Text from 'components/commons/elements/Text';
import InquiryLayout from 'components/commons/InquiryLayout/InquiryLayout';
import { DateType } from 'types/common-types';
import { formatDate } from 'shared/function';
import OperatingBox from 'components/factory/operatingHistory/OperatingBox';
import { operatingHistoryYK_Thunk } from 'store/modules/operatingHistory';
import { ThunkDispatchType } from 'store/modules';
import { setData, setEquipmentSize, setAllData } from 'store/modules/operatingHistory';
import { OperatingHistoryAPI } from "api/api";
import WorkingHour from 'components/commons/InquiryLayout/WorkingHour';
import useInquiryLayout from 'hooks/useInquiryLayout';

function OperatingHistory({initialData}:any) {
  const router = useRouter();
  const dispatch = useDispatch<ThunkDispatchType>();
  const [equipType, date, workingHour, setDate, setWorkingHour] =useInquiryLayout();
 
  const dataAll = useSelector(({operatingHistory}: RootState) => operatingHistory.data)
  
  // DESCRIBE: 컴포넌트 첫 렌더링을 catch하기 위해서
  const firstUpdate = useRef(true);

  // const { data, isLoading } = useQuery(['operatingHistoryYK', {startDate:date.startDate, 
  //                                                   endDate:date.endDate, 
  //                                                   workingHour:workingHour}
  //                           ], 
  //                           ()=> { OperatingHistory_YK(formatDate(date.startDate), 
  //                                                     formatDate(date.endDate), 
  //                                                     workingHour)},
  //                           { initialData:initialData, refetchOnWindowFocus: false,
  //                             onSuccess: data => {
  //                               console.log(data)
  //                             }
  //                           })

  // const operatingData = data ?? data;

  // DESCRIBE: query에서 뎁스 정보 가져와서 state 관리
  const selectedDataByDepth = (_data:any) => {
    const current_depth = router.query.depth ? router.query.depth : "company";
    const depthId = router.query.depthId ? parseInt(router.query.depthId as string) : null;
    let component_depth;

    switch(current_depth){
      case "company" : component_depth = "factory"; break;
      case "factory" : component_depth = "process"; break;
      case "process" : component_depth = "equipment"; break;
      // case "depth1" : component_depth = "equipment"; break;
      case "equipment" : component_depth = ""; break;
    }
    
    const index = _data.findIndex((f: (string | string[])[]) => f[0] === current_depth);
    const depthIndex = depthId ? _data[index][1].findIndex((f:any) => f.id === depthId) : 0;    
    const selected_data = _data[index][1][depthIndex];

    const payload = {
      data: selected_data,
      current_depth: current_depth,
      current_depthId: selected_data.id,
      component_depth: component_depth
    }

    dispatch(setAllData(payload));
  }

  // DESCRIBE: 첫 렌더링시에 작동해야하는 함수들
  useEffect(()=>{
    // 서버에서 받아온 Props를 redux에서 관리
    dispatch(setData(initialData));
    // redux로 옮긴 Props &  query depth로 data 세팅
    selectedDataByDepth(initialData);
  },[])

  // DESCRIBE: depth가 바뀔 때
  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      return;
    }
    // depth가 바뀌었을 때 상태값 변경
    selectedDataByDepth(dataAll);

  },[router.query.depth])

  // DESCRIBE: 설비 크기가 변했을 때 안에 함수 작동
  useEffect(()=>{
    // 변경된 equipmentSize redux로 상태관리
    dispatch(setEquipmentSize(equipType));
  },[equipType])


  // DESCRIBE: 조회 기간 또는 근무 시간 변경되었을 때 안에 함수 작동
  useEffect(()=>{
    requestData(workingHour, date);
  },[WorkingHour, date])


  // 조회 기간 또는 근무 시간 변경되었을 때 api 요청 및 상태 변경(Redux)
  const requestData = (workingHour:string, date:DateType) => {
    
    const params = {
      startDate : formatDate(date.startDate),
      endDate : formatDate(date.endDate),
      workingHour : workingHour
    }
    
    dispatch(operatingHistoryYK_Thunk(params))
  }
  
  return (
    <>
      <Head>
        <title>가동 히스토리</title>
      </Head>
      <Layout>
        <Text 
          title="가동 히스토리" 
          type="header1" 
          color="primary500" 
          weight="bold"
        />
        <InquiryLayout 
          timePicker 
          workingHour 
          equipmentType
          equipmentTypeData={equipType}
          workingHourData={workingHour}
          date={date}
        />
        <OperatingBox/>
      </Layout>
    </>
  )
}

export async function getStaticProps() {

  const date = {
    startDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()-1),
    endDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()-1)
  };

  const workingHour = 'all';

  // const queryClient = new QueryClient();

  // await queryClient.prefetchQuery('operatingHistoryYK', 
  //       ()=> OperatingHistory_YK(formatDate(date.startDate), formatDate(date.endDate), workingHour))

  const result = await OperatingHistoryAPI.yulkok(
                  formatDate(date.startDate),
                  formatDate(date.endDate),
                  workingHour
                )
                .then((res) => {
                  return Object.entries(res.results);
                })
                .catch((err) => {
                  console.log("가동 히스토리 데이터 패칭 오류");
                  return err;
                });

  const data = result;
  
  return {
    props : {
      // dehydratedState: dehydrate(queryClient),
      initialData: data
    },
    revalidate: 300,
  };
}


export default OperatingHistory;
