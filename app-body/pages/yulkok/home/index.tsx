import Layout from '../../../components/commons/EntireLayout/Layout';
import React , {useEffect, useRef, useState} from 'react';
import { useRouter } from "next/router";
import { formatDate } from '../../../shared/function';
import Router from "next/router";
import { useSelector } from "react-redux"
import { RootState } from '../../../store/modules';

import classes from "../../../components/commons/PasswordChangeModal/password-change-modal.module.scss";
import AlertInitialPWModal from "../../../components/commons/PasswordChangeModal/AlertInitialPWModal";
import PasswordChangeModal from '../../../components/commons/PasswordChangeModal/PasswordChangeModal';
import SuccessModal from '../../../components/commons/PasswordChangeModal/SuccessModal';

import FailureModal from '../../../components/commons/PasswordChangeModal/FailureModal';

function Home({seconds, minutes}:any) {
  const firstUpdate = useRef(true);
  const router = useRouter();
  const count= useRef(0);

  const isInitialPassword = useSelector((state: RootState): boolean => state.login.data.isInitialPassword);

  const [alertInitialPWModal, setAlertInitialPWModal] = useState<boolean>(true);
  const [passwordChangeModal, setPasswordChangeModal] = useState<boolean>(false);
  const [successModal, setSuccessModal] = useState<boolean>(false);
  const [failureModal, setFailureModal] = useState<boolean>(false);

  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      return;
    }
    // Router.reload();
    count.current += 1;
    console.log('리로드 된 횟수:',count);
  }, []);

  const handleClickRetry = () => {
    setFailureModal(false);
    setPasswordChangeModal(true);
  }

  return (
    <>
      <Layout>
        {/* <PageSelectLayout/> */}
        <div>Home</div>
        <div>{minutes + ":" + seconds}</div>
      </Layout>
      {
        isInitialPassword && (
          <div className={classes.modalOverlay}>
            {alertInitialPWModal && (
              <AlertInitialPWModal
              setAlertInitialPWModal={setAlertInitialPWModal}
              setPasswordChangeModal={setPasswordChangeModal}/>
            )}
            {passwordChangeModal && (
              <PasswordChangeModal
              setPasswordChangeModal={setPasswordChangeModal}
              setSuccessModal={setSuccessModal}
              setFailureModal={setFailureModal} />
            )}
            {successModal && (
              <SuccessModal type="done" text="비밀번호 변경이 완료되었습니다." />
            )}
            {failureModal && (
              <FailureModal text="일시적인 오류로 인해 비밀번호 변경을 완료하지 못했습니다." onClick={handleClickRetry} />
            )}
          </div>
        )
      }
    </>
  )
}

export async function getStaticProps() {
  const seconds = new Date().getSeconds();
  const minutes = new Date().getMinutes();

  return {
    props : {seconds, minutes},
    revalidate: 30,
  }

}

export default Home;
// 배포가자아아아아