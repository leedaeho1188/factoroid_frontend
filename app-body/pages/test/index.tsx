import React, {useCallback} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as counterActions from '../../store/modules/counter';

interface counterValue {
  counter: {
    value: number;
  };
}

export default function Test() {
    const dispatch = useDispatch();
    const value = useSelector(({ counter } :counterValue) => counter.value);

    const plus = useCallback(
      () => {
        dispatch(counterActions.increment());
      },
      [dispatch]
    );

    const minus = useCallback(
      ({ value }) => {
        dispatch(counterActions.decrement());
      },
      [dispatch]
    );

    return (
      <div>
        <h1>Counter</h1>
        <button onClick={minus}>-</button>
        <span>{value}</span>
        <button onClick={plus}>+</button>
      </div>
    );
}
