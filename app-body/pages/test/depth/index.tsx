import React from 'react'
import Table from '../../../components/commons/Table/Table';

export default function index() {

    const sample_data = Array.from(Array(5), () => Array(18).fill(1));

    const options = {
      header: [
        {
          rowSpan: [2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1],
          colSpan: [1, 1, 1, 1, 2, 2, 2, 2, 1, 3, 2],
          scope: [
            "rowgroup",
            "rowgroup",
            "rowgroup",
            "rowgroup",
            "colgroup",
            "colgroup",
            "colgroup",
            "colgroup",
            "colgroup",
            "colgroup",
            "colgroup",
          ],
          value: [
            "날짜",
            "소속 공장",
            "장비",
            "운용 시간",
            "가동",
            "비가동",
            "유휴",
            "정지",
            "예외가동",
            "식사 시간 중 가동 시간",
            "최초 동작 시간",
          ],
        },
        {
          value: [
            "가동률",
            "가동 시간",
            "비가동률",
            "유휴율",
            "유휴 시간",
            "정지율",
            "정지 시간",
            "예외 가동률",
            "예외 가동 시간",
            "야간",
            "점심",
            "저녁",
            "주간",
            "야간",
          ],
        },
      ],
      data: sample_data,
    };
    return (
      <div>
        <Table options={options}></Table>
      </div>
    );
}
