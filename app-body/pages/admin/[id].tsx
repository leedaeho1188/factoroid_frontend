import { useRouter } from 'next/router'

const Index = () => {
  const router = useRouter()
  const { id } = router.query

  return <p>Admin: {id}</p>
}

export default Index