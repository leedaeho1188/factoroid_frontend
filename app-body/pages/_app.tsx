import React, {useState} from 'react';
import '../styles/globals.scss'
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from "react-query/devtools";
import type { AppProps } from 'next/app';
import {RouteGuard} from '../RouteGuard';
import {wrapper} from '../store';
import Head from 'next/head';

function MyApp({ Component, pageProps }: AppProps) {

  const queryClientRef = React.useRef<QueryClient>();
  if (!queryClientRef.current) {
    queryClientRef.current = new QueryClient();
  }

  queryClientRef.current.setDefaultOptions({
    queries: {
      staleTime: Infinity,
    },
  });


  return (
    <>
      <Head>
        <link rel="manifest" href="/manifest.json" />
        <link rel="apple-touch-icon" href="/icon-192x192.png"></link>
        <meta name="theme-color" content="#fff" />
      </Head>
      <QueryClientProvider client={queryClientRef.current}>
        <Hydrate state={pageProps.dehydratedState}>
          <RouteGuard>
            <Component {...pageProps} />
          </RouteGuard>
        </Hydrate>
        <ReactQueryDevtools initialIsOpen></ReactQueryDevtools>
      </QueryClientProvider>
    </>
  );
}


export default wrapper.withRedux(MyApp);
