export const worst10MockData={
    "results": [
        {
            "id": 48,
            "name": "DMU50 #2",
            "operationHour": 0,
            "hourList": [
                0
            ],
            "realOperationDate": 0,
            "depthInfo": {
                "factoryId": 21,
                "factoryName": "사천",
                "processId": 47,
                "processName": "사천생산",
                "Depth1Name": "생산2파트-2",
                "Depth1Id": 4
            },
            "lossInfo": {
                "time": "0.000",
                "timeList": [
                    -999.0
                ],
                "rate": "0.000",
                "trend": [
                    "데이터 없음"
                ]
            },
            "offInfo": {
                "time": "0.000",
                "timeList": [
                    -999.0
                ],
                "rate": "0.000",
                "trend": [
                    "데이터 없음"
                ]
            }
        }
    ]
}