import { OperatingHistoryAPI } from "./api"


export const OperatingHistory_YK = async(startDate:string, endDate:string, workingHour:string) => {
    await OperatingHistoryAPI.yulkok(startDate, endDate, workingHour)
        .then((res:any) => {
            console.log(startDate, endDate, workingHour)
            console.log(Object.entries(res.data.results));
            return Object.entries(res.data.results);
        })
        .catch((err) => {
            console.log("가동 히스토리 데이터 패칭 오류");
            return err;
        });
}