// 고객 정보를 모아 두었습니다.

export const customersInfo = [
    {
        customer:{
            path: "yulkok",
            title: "율곡"
        },
        pages:[
            {
                path: "home",
                title: "Home"
            },
            {
                path: "operatingHistory", 
                title:"가동 히스토리"
            }, 
            {
                path: "worst10", 
                title:"worst10"
            }
        ]
    },
    {
        customer:{
            path: "daehan",
            title: "대한공조"
        },
        pages:[
            {
                path: "operatingHistory",
                title: "가동 히스토리"
            },
            {
                path: "operatingStatus",
                title: "가동 현황"
            }
        ]
    }
]