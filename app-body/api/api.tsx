import axios, {AxiosResponse, AxiosError} from 'axios';
import {LoginForm} from '../types/user/user-types';
import * as Sentry from "@sentry/browser";

export const api=axios.create({
    // baseURL:'https://thejjangictjjang.com', // stg 서버
    baseURL:'http://localhost:9090', // local 서버 
    // baseURL:'https://factoroid.gentleenergycorp.com/' //prod 서버
    // baseURL: 'http://52.231.13.82', // test 서버
    withCredentials:true,
});

const responseBody = (response: AxiosResponse) => response.data;

const handleError=(err: AxiosError | Error )=> Sentry.captureException(err);

export const UseAuth = {
  login: (data: LoginForm) =>
    api
      .post("api/login", null, {params: data})
      // .then(responseBody)
      // .catch(handleError),
      ,
  adminLogin: (data: LoginForm) =>
    api
      .post("/api/login", { withCredential: true }, { params: data })
      // .then(responseBody)
      // .catch(handleError),
      ,
  testLogin: (data: LoginForm) =>
    api
      .post("api/test/login", null, { params: data })
      // .then(responseBody)
      // .catch(handleError)
      ,
  adminDetail: (username: string) =>
    api
      .get("/api/admin-details", { params: { username: username } })
      .then(responseBody)
      .catch(handleError),
  currentLogin: () =>
    api
      .get("/api/account/current-login"),
  currentPasswordCheck: (accountId: number, password: string) =>
    api
      .post(`/api/account/${accountId}/password`, null, {params: { password: password }}),
  changePassword: (accountId: number, password: string) =>
    api
      .put(`/api/account/${accountId}/password`, {password: {value: password}})
};

export const OperatingHistoryAPI = {
  yulkok: (startDate: string, endDate: string, workingHour: string) =>
    api
      .get(`/api/company/operating/history/8?beginDate=${startDate}&endDate=${endDate}&timeType=${workingHour}`)
      .then(responseBody)
      .catch(handleError),
};

export const Worst10API = {
  yulkok: (beginDateTime: string, endDateTime: string, timeType: string) =>
    api
      .get(`api/etc/worst10/8?beginDateTime=${beginDateTime}&endDateTime=${endDateTime}&timeType=${timeType}`)
      .then(responseBody)
      .catch(handleError),
};