import axios from "axios";
import { api } from "../../shared/axios";

/**
 * 
 * @param startDate 
 * @param endDate 
 * @param workingHour 
 * @returns 
 */

export const operatingHistoryApi_YK = (startDate:string, endDate:string, workingHour:string) => {
    const result = api.get(
        `/api/company/operating/history/8?beginDate=${startDate}&endDate=${endDate}&timeType=${workingHour}`
    ).then((res)=>{
        const data = res.data.results;
        // console.log(data);
        return Object.entries(data);
    }).catch((err)=>{
        return err
    })
    return result
}
