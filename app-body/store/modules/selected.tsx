import { createSlice } from "@reduxjs/toolkit";

interface stateType{
    data:{
        [key:string]: boolean
    },
    selected: string
}

const initialState: stateType = {
  data: {
    loss: true,
    idle: false,
    off: false,
  },
  selected: "loss",
};

const selected = createSlice({
  name: "selected",
  initialState,
  reducers: {
    clickLoss: (state) => {
      state.data.loss = true;
      state.data.idle = false;
      state.data.off = false;
      state.selected="loss";
    },
    clickIdle: (state) => {
      state.data.loss = false;
      state.data.idle = true;
      state.data.off = false;
      state.selected = "idle";

    },
    clickOff: (state) => {
      state.data.loss = false;
      state.data.idle = false;
      state.data.off = true;
      state.selected = "off";
    },
  },
});

export const {
    clickLoss,
    clickIdle,
    clickOff,
}= selected.actions;

export default selected.reducer;