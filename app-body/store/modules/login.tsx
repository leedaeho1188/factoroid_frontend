import { AxiosResponse, AxiosError } from 'axios';
import { createAsyncThunk, createSlice, createAction } from "@reduxjs/toolkit";
import { UseAuth } from "../../api/api";
import {LoginForm, CurrentPasswordCheck, ChangePasswordForm } from "../../types/user/user-types";
import * as Sentry from "@sentry/browser";

export const LoginThunk = createAsyncThunk("fetch/login", (form: LoginForm) =>  {
  const result = UseAuth.login(form)
    .then((res: AxiosResponse) => {
      return {result:res.data.code, username:form.username, status: "success"};
    })
    .catch((err: AxiosError | Error ) => {
      Sentry.captureException(err);
      console.log("fetch/login 에러", err);
      const error: any = err;

      return {result: error.response.data.code, username: form.username, status:"failed", err: err};
    });

  return result;
});

export const CurrentLoginThunk = createAsyncThunk("fetch/current-login", () => {
  const result  = UseAuth.currentLogin()
    .then((res: AxiosResponse) => {
      return {result: res.data, status: "success"};
    })
    .catch((err: AxiosError | Error) => {
      Sentry.captureException(err);
      console.log("fetch/current-login 에러", err);
      const error: any = err;

      return {result: error.response.data, status: "failed", err: err};
    });

  return result;
});

export const CurrentPasswordCheckThunk = createAsyncThunk("fetch/current-password-check", (data: CurrentPasswordCheck) => {
  const result = UseAuth.currentPasswordCheck(data.accountId, data.password)
  .then((res) => {
    return {result: res.data}
  })
  .catch((err: AxiosError | Error) => {
    Sentry.captureException(err);
    console.log("fetch/current-password-check 에러", err);

    return err;
  })

  return result;
})

export const ChangePasswordThunk = createAsyncThunk("fetch/change-password", (data: ChangePasswordForm) => {
  const result = UseAuth.changePassword(data.accountId, data.password)
  .then((res) => {
    console.log(res.data)
    return {result: res.data}
  })
  .catch((err: AxiosError | Error) => {
    Sentry.captureException(err);
    console.log("fetch/change-password 에러", err);

    return err;
  })

  return result;
})

export const LoginStatusInit = createAction("init/login-status");
export const InitialPasswordCheck = createAction("check/initial-password", (status: boolean) => {
  return {
    payload: {
      status: status,
    }
  }
})


// DESCRIBE : admin 로그인과 관련된 상태값.
const LoginSlice = createSlice({
  name: "login",
  initialState: {
    data: 
    {
        accountId: null,
        username:'',
        name: "",
        department: "",
        email: "",
        phone: "",
        isGEC: null,
        isSuper: null,
        type:'',
        isLogin: false,
        isInitialPassword: false,
    },
    status: "",
    customedCode: "",
  },
  reducers: {
    // standard reducer logic, with auto-generated action types per reducer
  },
  extraReducers: (builder) => {
      builder
        .addCase(LoginThunk.pending, (state, action) => {
            state.status="loading";
        })
        .addCase(LoginThunk.fulfilled, (state, {payload}:any)=>{
            const {result, username}=payload;
            if (result === -107 || result === -108) {
              state.data.username = username;
              state.data.type = result === -107 ? "customer" : "admin";
              state.data.isLogin = true;
              state.status = "success";
              state.customedCode = result;
            }
            else if (result === -101 || result === -102) {
              state.status = "failed";
              state.customedCode = result;
            }
        })
        .addCase(LoginThunk.rejected, (state, {payload}:any)=>{
            state.status="rejected";
        })
        .addCase(LoginStatusInit.type, (state, action) => {
          state.status = "";
          state.customedCode = "";
        })
        .addCase(CurrentLoginThunk.pending, (state, action) => {
          state.status = "loading";
        })
        .addCase(CurrentLoginThunk.fulfilled, (state, {payload}: any) => {
          // $TODO: action은 fulfilled인데 state가 교체 되지 않음, 확인 필요
          const {result} = payload;
          if (result.code === 200) {
            state.status = "success";
            state.customedCode = result.code;
            state.data.accountId = result.data.id;
            state.data.name = result.data.name;
            state.data.username = result.data.username;
            state.data.department = result.data.department;
            state.data.email = result.data.email;
            state.data.phone = result.data.phone;
            state.data.isGEC = result.data.isGEC;
            state.data.isSuper = result.data.isSuper;
          } else if (result.code === -111) {
            state.status = "failed";
            state.customedCode = result.code;
          }
        })
        .addCase(CurrentLoginThunk.rejected, (state, {payload}: any) => {
          state.status = "rejected";
        })
        .addCase(InitialPasswordCheck.type, (state, {payload}: any) => {
          state.data.isInitialPassword = payload.status;
        })
        .addCase(CurrentPasswordCheckThunk.pending, (state, action) => {
          state.status = "pw-loading";
        })
        .addCase(CurrentPasswordCheckThunk.fulfilled, (state, {payload}: any) => {
          const {result} = payload;
          state.customedCode = result.code;

          if (result.code === 200) {
          state.status = "pw-loading";
          }
          else if (result.code === -102 || result.code === -109) {
            state.status = "failed";
          }
        })
        .addCase(CurrentPasswordCheckThunk.rejected, (state, {payload}: any) => {
          state.status = "failed";
        })
        .addCase(ChangePasswordThunk.pending, (state, action) => {
          state.status = "pw-loading";
        })
        .addCase(ChangePasswordThunk.fulfilled, (state, {payload}: any) => {
          const {result} = payload;
          state.customedCode = result.code;

          if (result.code === 200) {
            state.status = "success";
          }
          else if (result.code === -104 || result.code === -109) {
            state.status = "failed";
          }
        })
        .addCase(ChangePasswordThunk.rejected, (state, {payload}: any) => {
          state.status = "failed";
        })
  }
});

export default LoginSlice.reducer;