import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { UseAuth } from "../../api/api";

// DESCRIBE : 어드민 로그인 Thunk
export const adminLoginThunk  = createAsyncThunk(
  'fetch/adminLogin',
  async(form:any)=>{
      const result= UseAuth
      .adminLogin(form)
      .then((res)=>{
          return res.data;
      })
      .catch((err)=>{
        console.log('fetch/adminLogin 에러', err);
        return false;
      })
      return result;
  }
)

// DESCRIBE : 어드민 정보 가져오는 Thunk
export const adminDetailThunk = createAsyncThunk(
  'fetch/adminDetail',
  async(username:string)=>{
    const result= UseAuth
    .adminDetail(username)
    .then((res)=>{
      return res.data;
    })
    .catch((err)=>{
      console.log('fetch/adminDetail 에러', err);
      return false;
    })

    return result;
  }
)

// DESCRIBE : admin 로그인과 관련된 상태값.
const adminLoginSlice = createSlice({
  name: "admin",
  initialState: {
    data: {
      adminInfo: {
        dashboardMainFactory1: 0,
        dashboardMainFactory2: 0,
        dashboardMainFactory3: 0,
        id: 0,
        inactive: false,
        inactiveDate: null,
        name: "",
        readOnly: false,
        regDate: "",
        username: "",
      },
      isLogin: false,
    },
    status: "",
  },
  reducers: {
    // standard reducer logic, with auto-generated action types per reducer
  },
  extraReducers: (builder) => {
    builder
      .addCase(adminLoginThunk.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(adminLoginThunk.fulfilled, (state, { payload }: any) => {
        state.data.isLogin = payload;
        state.status = "success";
      })
      .addCase(adminLoginThunk.rejected, (state, action) => {
        state.status = "failed";
      })
      .addCase(adminDetailThunk.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(adminDetailThunk.fulfilled, (state, { payload }: any) => {
        state.data.adminInfo = payload;
        state.status = "success";
      })
      .addCase(adminDetailThunk.rejected, (state, action) => {
        state.status = "failed";
      });
  },
});

export default adminLoginSlice.reducer;