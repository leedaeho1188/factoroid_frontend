import {  Action, ThunkDispatch, combineReducers } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

import counter from './counter';
import admin from './admin';
import operatingHistory from './operatingHistory'

import depthInfo from "./depthInfo";
import selected from "./selected";
import worst10 from './worst10';
import login from './login';

const reducer = (state:any, action:any) => {
  if (action.type == HYDRATE) {
    return {
      ...state,
      ...action.payload,
    };
  }
  return combineReducers({
    counter,
    admin,
    operatingHistory,
    depthInfo,
    selected,
    worst10,
    login
    // 여기에 추가
  })(state, action);
};

export type RootState = ReturnType<typeof reducer>;
export type ThunkDispatchType = ThunkDispatch<RootState, void, Action>;
export default reducer;
//test