import { createSlice } from "@reduxjs/toolkit";

interface DepthInfo {
  [key: number]: {
    checked: boolean;
    depth: string;
    display: string;
    lowerDepth: number[];
    name: string;
    state_id: number;
    upperDepth: number[];
    total_checked_num: number;
    unchecked_num: number;
    checked_num: number;
    section_display:string;
  };
}

interface stateType{
    data: DepthInfo
}
const initialState:stateType ={
    data:[],

}

const depthInfo = createSlice({
  name: "depthInfo",
  initialState,
  reducers: {
    // DESCRIBE : 뎁스 정보 추가
    addDepthInfo: (state, { payload }) => {
      state.data[payload.state_id] = payload;
    },
    // DESCRIBE : 프로세스 단위에서 실행되는 체크박스 리듀셔
    handleProcessChecked: (state, { payload }) => {
      const selected_id = payload.id;
      const checked = payload.checked;
      const display = checked === true ? "block" : "none";
      changeStatus(state, selected_id, checked, display);
      state.data[selected_id].lowerDepth.map((lower_id: number) => {
        changeStatus(state, lower_id, checked, display);
      });
    },
    // DESCRIBE : 공장 단위에서 실행되는 체크박스 리듀서
    handleFactoryChecked: (state, { payload }) => {
      const selected_id = payload.id;
      const selected_id_list = payload.id_list;
      const checked = payload.checked;
      const display = checked === true ? "block" : "none";

      // TODO: 1. 공장 하위에 있는 체크 박스 상태 변경
      changeStatus(state,selected_id,checked,display);
      selected_id_list.map((id_x: number) => {
        changeStatus(state, id_x, checked, display);
        let lower_id_list = state.data[id_x].lowerDepth;
        lower_id_list.map((id_y: number) => {
          changeStatus(state, id_y, checked, display);
        });
      });
    },
    handleBaseChecked: (state, { payload }) => {
      const selected_id = payload.id;
      const checked = payload.checked;
      state.data[selected_id].checked = checked;
    },
    handlePartChecked: (state, { payload }) => {
      const selected_id = payload.id;
      const checked = payload.checked;
      state.data[selected_id].checked = checked;
    },
    handleUpperChecked: (state, { payload }) => {
      const selected_id = payload.id;
      const lower_id = payload.lower_id;
      const checked = payload.checked;
      state.data[selected_id].checked = checked;
      state.data[lower_id].checked = checked;
    },
    addCheckedInfo: (state, { payload }) => {
      const state_id = payload.state_id;
      try{
        state.data[state_id].total_checked_num = payload.total_checked_num;
        state.data[state_id].checked_num = payload.checked_num;
        state.data[state_id].unchecked_num = payload.unchecked_num;
        state.data[state_id].section_display = payload.section_display;
      }
      catch{
        console.log('여기서 오류 : ', state_id);
      }
    },
    plusCheckedInfo: (state, { payload }) => {
      try{
        const upper_id = payload.upper_id;
        plusNum(state,upper_id);
      }
      catch{
        console.log('여기서 오류 :', payload.upper_id);
      }
    },
    minusCheckedInfo: (state, { payload }) => {
      const upper_id = payload.upper_id;
      minusNum(state,upper_id);
    },
    getCheckedNumber: (state, { payload }) => {
      const this_id = payload.upper_id;
      const upper_id = state.data[this_id].upperDepth[0];
      const checked_num = state.data[this_id].checked_num;
      const total_num = state.data[this_id].total_checked_num;

      console.log('지금 위의 아이디 :', this_id, '체크 된 개수 :',checked_num, '전체 개수 :', total_num);
      // TODO: 1. 해당 섹션 확인
      if (checked_num == 0) {
        state.data[this_id].display = "none";
        state.data[this_id].checked=false;
        minusNum(state, upper_id);
      } else if (checked_num == total_num) {
        state.data[this_id].display = "block";
        state.data[this_id].checked = true;
      }

      // TODO: 2. 해당 섹션 또한 모두 비어 있는 경우
      if(upper_id!=undefined){
        const upper_checked_num = state.data[checked_num].checked_num;
        const upper_total_num = state.data[checked_num].total_checked_num;
        
        if (upper_checked_num == 0) {
          state.data[upper_id].checked = false;
        } else if (upper_checked_num == upper_total_num) {
          state.data[upper_id].checked = true;
        }
      }
    },
    displayLowerDepth: (state, { payload }) => {
      const this_id = payload.this_id;
      const upper_id = payload.upper_id;
      const lower_list = payload.lower_id_list;
      const item = state.data[this_id];
      if (lower_list.includes(item.state_id) || item.state_id == this_id) {
        state.data[this_id].display = "block";
        plusNum(state,this_id);
      }
    },
  },
});

function changeStatus(state: any,id:number, checked: boolean, display:string){
   state.data[id].checked = checked;
   state.data[id].display = display;
   state.data[id].checked_num = state.data[id].total_checked_num;
   state.data[id].unchecked_num = 0;
}

function plusNum(state :any ,id :number){
  state.data[id].checked_num =
    state.data[id].checked_num < state.data[id].total_checked_num
      ? state.data[id].checked_num + 1
      : state.data[id].checked_num;
  state.data[id].unchecked_num =
    state.data[id].total_checked_num - state.data[id].checked_num;
}

function minusNum(state: any, id: number) {
  state.data[id].checked_num =
    state.data[id].checked_num > 0
      ? state.data[id].checked_num - 1
      : state.data[id].checked_num;
  state.data[id].unchecked_num =
    state.data[id].total_checked_num - state.data[id].checked_num;
}

export const {
  addDepthInfo,
  handleFactoryChecked,
  handleProcessChecked,
  handlePartChecked,
  handleUpperChecked,
  handleBaseChecked,
  addCheckedInfo,
  plusCheckedInfo,
  minusCheckedInfo,
  getCheckedNumber,
  displayLowerDepth,
} = depthInfo.actions;
export default depthInfo.reducer;