import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Worst10API } from "../../api/api"; 
import { Worst10Object } from "../../types/yulkok/worst10-types";

interface Worst10Params{
    startDate:string,
    endDate:string,
    workingHour:string,
}

interface DummyObject{
    time: number,
    rate: string,
    timeList: Array<number>,
    trend: Array<number>,
}

export const worst10Thunk = createAsyncThunk(
    "fetch/worst10",
    async (worst10Params: Worst10Params) => {
      const { startDate, endDate, workingHour } = worst10Params;
      const result= Worst10API
      .yulkok(startDate, endDate, workingHour)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log("fetch/worst10 에러", err);
        return false;
      });
      return result;
    }
);

const worst10Slice=createSlice({
    name:"worst10",
    initialState:{
        data:[{}],
        status:"",
    },
    reducers:{
        initializeData:(state,{payload})=>{
            state.data=handleException(payload);
        }
    },
    extraReducers:(builder)=>{
        builder
          .addCase(worst10Thunk.pending, (state, action) => {
            state.status = "loading";
          })
          .addCase(worst10Thunk.fulfilled, (state, { payload }: any) => {
            state.data = handleException(payload.results);
            state.status = "success";
          })
          .addCase(worst10Thunk.rejected, (state, action) => {
            state.status = "failed";
          });
    }
})
export const { initializeData } = worst10Slice.actions;
export default worst10Slice.reducer;


// DESCRIBE : Worst10 장비 데이터 예외처리 함수
export function handleException(equipments: Worst10Object[]) : Worst10Object[] {
  let checkKeyList: string[]=['lossInfo', 'offInfo', 'idleInfo'];
  let result:Worst10Object[]=[];
  
  // DESCRIBE : 데이터 리스트에서 해당 키값을 가지고 있지 않은 데이터 찾아서 더미로 채워주기.
  equipments.map((equipment: Worst10Object, i: number) => {
    checkKeyList.map((ck)=>{ checkKeyExists(ck, equipment, result);})
  });

  return result;
}

// DESCRIBE : 해당 장비 데이터에 키값이 있는지 확인. 없다면, 더미 객체 추가
function checkKeyExists(checkKey:string, equipment:Worst10Object, result:Worst10Object[]) {
  let newEquipment = equipment;

  !newEquipment.hasOwnProperty(checkKey)
    ? (newEquipment = {
        ...newEquipment,
        [checkKey]: makeDummyData(getListLength(equipment)),
      })
    : "";

  result.push(newEquipment);
}


// DESCRIBE : 더미 데이터 만드는 함수
function makeDummyData(listLength:number): DummyObject {
  return {
    time: 0,
    rate: "0",
    timeList: new Array(listLength, -999),
    trend: new Array(listLength),
  };
}

// DESCRIBE: 조회 기간에 따른 배열 길이 알아내는 함수 
function getListLength(equipment: Worst10Object) {
  return equipment.hourList.length;
}



