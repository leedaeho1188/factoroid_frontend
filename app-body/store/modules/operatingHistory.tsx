import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { OperatingHistoryAPI } from "../../api/api";

type operatingHistoryParams = {
    startDate : string,
    endDate : string,
    workingHour: string,
}

// DESCRIBE: OperatingHistory Thunk
export const operatingHistoryYK_Thunk = createAsyncThunk(
    'fetch/operatingHistoryYK',
    async(params:operatingHistoryParams, {dispatch})=>{
        try{
            const response = await OperatingHistoryAPI.yulkok(params.startDate, params.endDate, params.workingHour)
                                    .then((res)=>{
                                        const data = res.results;
                                        return Object.entries(data);
                                    })
                                    .catch((err)=> {
                                        console.log('가동히스토리 api 요청이 제대로 이루어지지 않았습니다.');
                                        return err;
                                    })
            dispatch(setData(response));
            dispatch(setSelectedData(response));
            return response;
        }
        catch(err:any){
            return err.response.data;
        }
    }

)

const operatingHistoryYK_Slice = createSlice({
    name: "operatingHistory",
    initialState:{
        data:[
            ["company", []],
            ["factory", []],
            ["process", []],
            ["depth1", []],
            ["equipment", []]
        ],
        status: "",
        selected_data:{
            infoBySize:{
                all: {
                    runInfo:{
                        runUp :'-',
                        idle: '-',
                        runDown: '-',
                        off : '-',
                        exceptionRunUp:'-'
                    }
                },
                large: {
                    runInfo:{
                        runUp :0,
                        idle: 0,
                        runDown: 0,
                        off : 0,
                        exceptionRunUp:0
                    }
                },
                medium: {
                    runInfo:{
                        runUp :0,
                        idle: 0,
                        runDown: 0,
                        off : 0,
                        exceptionRunUp:0
                    }
                },
                small: {
                    runInfo:{
                        runUp :0,
                        idle: 0,
                        runDown: 0,
                        off : 0,
                        exceptionRunUp:0
                    }
                }
            }
        },
        current_depth:"company",
        current_depthId : 8,
        component_depth:"factory",
        equipmentSize: "all" 
    },
    reducers: {
        setData: (state, {payload})=> {
            state.data = payload;
        },
        setEquipmentSize: (state, {payload}) => {
            state.equipmentSize = payload;
        },
        setComponentDepth : (state, {payload}) => {
            state.component_depth = payload;
        },
        setAllData : (state, {payload}) => {
            state.selected_data = payload.data;
            state.current_depth = payload.current_depth;
            state.current_depthId = payload.current_depthId;
            state.component_depth = payload.component_depth;
        },
        setCurrentDepth : (state, {payload}) => {
            state.current_depth = payload
        },
        setSelectedData : (state, {payload}) => {
            const index = payload.findIndex((f: (string | string[])[]) => f[0] === state.current_depth);
            const depthIndex = state.current_depthId ? payload[index][1].findIndex((f:any) => f.id === state.current_depthId) : 0;    
            const selected_data = payload[index][1][depthIndex];
            state.selected_data = selected_data;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(operatingHistoryYK_Thunk.pending, (state, action)=> {
                state.status = "loading";
            })
            .addCase(operatingHistoryYK_Thunk.fulfilled, (state, { payload }: any)=>{
                state.status = "success";
            })
            .addCase(operatingHistoryYK_Thunk.rejected, (state,action)=> {
                state.status = "failed";
            })
    }
})


export const {
    setData,
    setAllData,
    setEquipmentSize,
    setComponentDepth,
    setSelectedData,
    setCurrentDepth,
} = operatingHistoryYK_Slice.actions


export default operatingHistoryYK_Slice.reducer;