export interface depthInfo {
  factoryId: number;
  factoryName: string;
  processId: number;
  processName: string;
  Depth1Name: string;
  Depth1Id: number;
}

export interface TrendInfo {
  time: number;
  rate: string;
  timeList:Array<number>;
  trend: Array<number>;
}
export interface Worst10ObjectProps {
  data: {
    id: number;
    name: string;
    depthInfo: depthInfo;
    lossInfo: TrendInfo;
    idleInfo: TrendInfo;
    offInfo: TrendInfo;
    activeTime: number;
    dayFirstActiveTimeAvg: string;
    nightFirstActiveTimeAvg: Array<string>;
  },
  rank: number
}
export interface Worst10Object {
  id: number;
  name: string;
  hourList: Array<number>;
  depthInfo: depthInfo;
  lossInfo: TrendInfo;
  idleInfo: TrendInfo;
  offInfo: TrendInfo;
  activeTime: number;
  dayFirstActiveTimeAvg: string;
  nightFirstActiveTimeAvg: Array<string>;
}

export interface Worst10Props {
  equipments: Array<Worst10Object>;
}

export interface CardListContainerProps{
  equipments: Array<Worst10Object>;
  status: string;
}

export interface CardListProps {
  children: any;
  title: string;
  selectButtonShow: boolean;
}
export interface Worst10Type {
  worst10: {
    data: Worst10Object[];
    status: string;
  };
}

export interface SelectButtonProps {
  selectButtonShow: boolean;
}
export interface SelectedType {
  selected: {
    data: {
      [key: string]: boolean;
    };
  };
}

