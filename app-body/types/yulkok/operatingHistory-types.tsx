export interface operatingHistoryDataTypes {
    data: []
}

export interface selected_dataTypes {
    id: number,
    name: string,
    infoBySize: any| {
        all : {
            runInfo:any| runInfoTypes,
            equipmentOperationStatusTypes: equipmentOperatingStatusTypes
        },
        large?: {
            runInfo:any| runInfoTypes,
            equipmentOperationStatusTypes: equipmentOperatingStatusTypes
        },
        medium?: {
            runInfo:any| runInfoTypes,
            equipmentOperationStatusTypes: equipmentOperatingStatusTypes
        },
        small?: {
            runInfo:any| runInfoTypes,
            equipmentOperationStatusTypes: equipmentOperatingStatusTypes
        }
    }
}

export interface runInfoTypes{
    runUp :number|string,
    idle: number|string,
    runDown: number|string,
    off : number|string,
    exceptionRunUp:number|string
}

export interface equipmentOperatingStatusTypes {
    total: number|string,
    operation: number|string,
    off: number|string,
    operatingRate: number|string
}

export interface OperationAvgBoxProps {
    selected_data : selected_dataTypes,
    equipmentSize : string,
    current_depth : string,
}

export interface EquipOperationBoxProps {
    selected_data : selected_dataTypes,
    equipmentSize : string
}

export interface EquipProductionBoxProps {
    selected_data : selected_dataTypes,
    equipmentSize : string
}

export interface ComponentDepthFilterProps {
    component_depth: string;
    current_depth: string;
}

export interface ComponentDepthBoxProps {
    data : any;
    equipmentSize: string;
    current_depth : string;
    component_depth: string;
}