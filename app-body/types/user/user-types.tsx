export interface User {
  dashboardMainFactory1: number;
  dashboardMainFactory2: number;
  dashboardMainFactory3: number;
  id: number;
  inactive: boolean;
  inactiveDate: null;
  name: string;
  readOnly: boolean;
  regDate: string;
  username: string;
}

export interface LoginForm{
  username:string,
  password:string
}
export interface Login{
    username:string;
    isLogin: boolean,
}

export interface CurrentPasswordCheck {
  accountId: number;
  password: string;
}

export interface ChangePasswordForm extends CurrentPasswordCheck {

}