export interface depthData {
    id:number;
    factory: string
    data: Array<Depth1>,
}
export interface Depth1 {
  id: number;
  state_id?: number;
  name: string;
  processList: Array<Depth2>;
  lowerDepth?: Array<Depth2>;
  upperDepth?:Array<number>;
}

export interface Depth2 {
    id: number;
    state_id?: number;
    name: string;
    depth1List: Array<Depth3>;
    upperDepth?: Depth1;
    lowerDepth?: Array<Depth3>;
}

export interface Depth3{
    id:number;
    state_id?: number;
    name:string;
    upperDepth? : Depth2;
}