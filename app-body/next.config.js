/** @type {import('next').NextConfig} */

const path = require('path');
const withImages = require('next-images')
const withSass = require('@zeit/next-sass');
const withPWA = require("next-pwa");
const withPlugins = require("next-compose-plugins");
const runtimeCaching = require('next-pwa/cache');

const config ={
  trailingSlash: true,
  images : {
    loader:'akamai',
    path:'http://localhost:3000',
    // path:'https://mango-ocean-0580a3700.azurestaticapps.net/',
    domains: ['localhost']
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
    reactStrictMode: true,
  },
  env:{
    MY_ENV : process.env.MY_ENV
  },
  
}

module.exports=withPlugins([withPWA,
    {pwa: {
      dest: "public",
      register: true,
      skipWaiting: true,
      runtimeCaching,
      disable: process.env.NODE_ENV === 'development',
    }}
], config)

