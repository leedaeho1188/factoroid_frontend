import { useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import classes from "./login.module.scss";
import LoginForm from "./LoginForm";
import FactoroidBI from "../../public/images/FactoroidBI.png";

import { useDispatch } from "react-redux";
import { ThunkDispatchType } from "../../store/modules/index";
import { LoginThunk } from "../../store/modules/login";

const Login = () => {
  const [wrong, setWrong] = useState<boolean>(false);

  const router = useRouter();
  const dispatch = useDispatch<ThunkDispatchType>();

  const onSubmit = async (form: { username: string; password: string }) => {
    const loginState = await dispatch(LoginThunk(form)).unwrap();
    const {result, username}= loginState;

    result
    ? checkLoginStatus(result)
    : setWrong(true);

    return result;
  };

  const checkLoginStatus=(result:string)=>{
    result.includes("login_success")
    ? handleRouting(result)
    : setWrong(true)
  }

  const handleRouting=(result:string)=>{
    const type = result.split(" : ")[1];
    type == "admin" ? router.push("/admin") : router.push("yulkok/home");
  }


  return (
    <div className={classes.login}>
      <div className={classes.login_content}>
        <div className={classes.login_form}>
          <div className={classes.image}>
            <Image src={FactoroidBI} />
          </div>
          {wrong && (
            <div className={classes.error}>
              가입하지 않은 아이디거나, 잘못된 비밀번호 입니다
            </div>
          )}
          <LoginForm />
        </div>
      </div>
    </div>
  );
};

export default Login;
