import React, {useState, useEffect} from 'react';
import classes from './login.module.scss';
import classNames from "classnames";
import Input from '../../components/commons/elements/Input'

import { useDispatch, useSelector } from "react-redux"
import { useRouter } from "next/router"
import { RootState, ThunkDispatchType } from "../../store/modules/index";
import { LoginThunk, LoginStatusInit, InitialPasswordCheck } from "../../store/modules/login";

import ResetBeforeLoginModal from "../commons/PasswordChangeModal/ResetBeforeLoginModal"
import FailureModal from "../commons/PasswordChangeModal/FailureModal"

interface LoginFormProps {
  onSubmit: (form: { username: string; password: string}) => void;
}

// function LoginForm ({onSubmit}: LoginFormProps) {  
function LoginForm () {  
  
    const router = useRouter();
    const dispatch = useDispatch<ThunkDispatchType>();

    const [wrong, setWrong] = useState<boolean>(false);
    const [usernameInput, setUsernameInput]=useState("default");
    const [passwordInput, setPasswordInput]=useState("default");
    const [buttonStatus, setButtonState]=useState("inactive");
    const [form, setForm] = useState({
      username: "",
      password: "",
    });

    const [loginStatus, setLoginStatus] = useState({username: "default", password: "default"});
    const [helperText, setHelperText] = useState({username: "", password: ""});
    const [resetBefromLoginModal, setResetBeforeLoginModal] = useState<boolean>(false);

    const storedStatus = useSelector((state: RootState) => state.login.status);

    useEffect(() => {
      if (storedStatus === "rejected") setWrong(true);
    }, [storedStatus])

    useEffect(()=>{
      if(usernameInput==="active" && passwordInput ==="active"){
        setButtonState("active");
      }
      else{
        setButtonState("inactive");
      }
    },[usernameInput, passwordInput])

    const { username, password } = form;

    // const onChange =( e : React.ChangeEvent<HTMLInputElement>) =>{
    //     const {name, value}= e.target;
    //     if(value.length > 0){
    //       name === "username" ? setUsernameInput("active") : setPasswordInput("active");
    //     }
    //     setForm ({
    //         ...form,
    //         [name]:value
    //     });
    // };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>)=>{
        e.preventDefault();

        if(buttonStatus==="inactive"){
          return
        
        }
        onSubmit(form)

        // setForm({
        //   username: "",
        //   password: "",
        // }); // 초기화
        // setUsernameInput("default");
        // setPasswordInput("default");
    }

    // const handleOnFocus=(e : React.FocusEvent<HTMLInputElement>)=>{
    //   const {name}=e.target;
    //   console.log(name);
    //   name === "username" 
    //   ? setUsernameInput("active")
    //   : setPasswordInput("active");
      
    //   console.log('handle on focus');
    // }

    // const handleOnBlur=(e : React.FocusEvent<HTMLInputElement>)=>{
    //   const {name}=e.target;
    //   name === "username" 
    //   ? username.length > 0 ? setUsernameInput("active") : setUsernameInput("default") 
    //   : password.length > 0 ? setPasswordInput("active") : setPasswordInput("default");
    //   console.log(name);
    //   console.log('handle on b');
    // } 

    const handleClickPWBtn=(e:React.MouseEvent<HTMLButtonElement>)=>{
       // DESCRIBE : 비밀번호 찾기 모달 띄우기
       e.preventDefault();
       e.stopPropagation();

       setResetBeforeLoginModal(true);
    }

    const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
      const {name, value} = e.target;
      if(value.length > 0){
        if (loginStatus.username === "danger") {
          setLoginStatus({...loginStatus, username: "default"})
          setHelperText({...helperText, username: ""})
        }
        if (loginStatus.password === "danger") {
          setLoginStatus({...loginStatus, password: "default"})
          setHelperText({...helperText, password: ""})
        }
        name === "username" ? setUsernameInput("active") : setPasswordInput("active");
      } else {
        name === "username" ? setUsernameInput("default") : setPasswordInput("default");
      }
      setForm({
        ...form,
        [name]: value,
      })
    };

    const onSubmit = async (form: { username: string; password: string }) => {
      const loginState = await dispatch(LoginThunk(form)).unwrap();
      const {result, status} = loginState;

      status === "success"
      ? checkLoginSuccess(result)
      : checkLoginFailure(result)
    };
  
    const checkLoginSuccess=(result:number)=>{
      // DESCRIBE: 커스텀 코드 -107 : account 로그인 성공
      // DESCRIBE: 커스텀 코드 -108 : admin 로그인 성공
      if (result === -107 || result === -108) {
        if (username === password) {
          dispatch(InitialPasswordCheck(true));
        }
        setForm({username: "", password: ""});
        setUsernameInput("default");
        setPasswordInput("default");
        setLoginStatus({username: "default", password: "default"});
        setHelperText({username: "", password: ""});
        handleRouting(result);
      }
    }

    const checkLoginFailure = (result: number | undefined) => {
      console.log(result)
      // DESCRIBE: 커스텀 코드 -101 : 존재하지 않는 username, 로그인 실패
      if (result === -101) {
        setLoginStatus({username: "danger", password: "default"});
        setHelperText({username: "등록되지 않은 ID입니다.", password: ""});
      }
      // DESCRIBE: 커스텀 코드 -102 : 잘못된 password, 로그인 실패
      if (result === -102) {
        setLoginStatus({username: "default", password: "danger"});
        setHelperText({username: "", password: "잘못된 비밀번호입니다."});
      }
    }
  
    const handleRouting=(result:number)=>{
      result === -108 ? router.push("/admin") : router.push("yulkok/home");
    }

    const handleClickRetry = () => {
      setWrong(false);
      // DESCRIBE: store에 저장되어 있는 status, customedCode 초기화
      dispatch(LoginStatusInit());
    }

    return (
      <>
        <form onSubmit={handleSubmit} className={classes.form}>
          <div className={classes.row}>
            <div className={classes.input_group}>
              {/* <label htmlFor="username" className={classNames(classes.label, classes[usernameInput])}>ID</label>
              <input
                type="text"
                name="username"
                value={username}
                onChange={onChange}
                className={classNames(classes.input, classes[usernameInput])}
                placeholder="ID"
                onFocus ={handleOnFocus}
                onBlur={handleOnBlur}
              /> */}
              <Input
              type="text"
              name="username"
              placeholder="ID"
              label="ID"
              value={form.username}
              status={loginStatus.username}
              helperText={helperText.username}
              size="medium"
              width="424px"
              onChange={handleOnChange} />
            </div>
          </div>
          <div className={classes.row}>
            <div className={classes.input_group} >
              {/* <label htmlFor="password" className={classNames(classes.label, classes[passwordInput])}>비밀번호</label>
              <input
                type="password"
                name="password"
                value={password}
                onChange={onChange}
                className={classNames(classes.input, classes[passwordInput])}
                placeholder="비밀번호"
                onFocus={handleOnFocus}
                onBlur={handleOnBlur}
              /> */}
              <Input
              type="password"
              name="password"
              placeholder="비밀번호"
              label="비밀번호"
              value={form.password}
              status={loginStatus.password}
              helperText={helperText.password}
              size="medium"
              width="424px"
              onChange={handleOnChange} />
            </div>
          </div>
          <div className={classes.row}>
            <button type="submit" className={classNames(classes.button, classes[buttonStatus])}>
              로그인
            </button>
          </div>
          <button className={classes.findPWBtn} onClick={handleClickPWBtn}>비밀번호가 기억나지 않으신가요?</button>
        </form>
        {wrong && (
          <FailureModal text="일시적인 오류로 인해 고객님의 계정에 로그인하지 못했습니다." onClick={handleClickRetry} />
        )}
        {resetBefromLoginModal && (
          <ResetBeforeLoginModal setResetBeforeLoginModal={setResetBeforeLoginModal} />
        )}
      </>
    );
}

export default LoginForm;