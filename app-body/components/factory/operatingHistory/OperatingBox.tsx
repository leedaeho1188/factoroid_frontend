import React from "react";
import { useSelector } from "react-redux";

import classes from './operatingHistory.module.scss'
import { RootState } from "../../../store/modules";
import DetailBox from "./DetailBox";
import ComponentBox from "./ComponentBox";
import DepthTitleBox from "./DepthTitleBox";



const OperatingBox = () => {

    const current_depth = useSelector(({operatingHistory}: RootState) => operatingHistory.current_depth);

    return(
        <div className={classes.operatingBox_container} >
            <DepthTitleBox/>
            <DetailBox />
            {/* depth가 equipment일때 componentBox rendering 안하게 하기 */}
            {current_depth === "equipment"
                || <ComponentBox />
            }
        </div>
    )
}
// 배로

export default OperatingBox;