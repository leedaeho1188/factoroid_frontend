import React from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

import Text from "../../commons/elements/Text";
import StickChart from "../../charts/StickChart";
import classes from './operatingHistory.module.scss';
import { ComponentDepthBoxProps } from "../../../types/yulkok/operatingHistory-types";

/**
 * 
 * @param data 율곡 구성 요소 각각 박스에 들어가는 data
 * @param equipmentSize 크기별 설비 그룹
 * @param current_depth 가동히스토리 페이지 뎁스
 * @param component_depth 구성 요소 뎁스
 */

const ComponentDepthBox = ({
    data,
    equipmentSize,
    current_depth,
    component_depth,
}:ComponentDepthBoxProps) => {
    const router = useRouter();

    const runInfo = component_depth === "equipment" ? data.infoBySize['all'].runInfo : data.infoBySize[equipmentSize].runInfo;

    const runInfoPercent = [runInfo.runUp === "-" ? 0 : runInfo.runUp, runInfo.idle === "-" ? 0 : runInfo.idle, runInfo.runDown === "-" ? 0 : runInfo.runDown, runInfo.off === "-" ? 0 : runInfo.off, runInfo.exceptionRunUp === "-" ? 0 : runInfo.exceptionRunUp];

    const doughnutColor = ['#116d38', '#ffc600', '#ff9f8b', '#f25341', "#dfdfe0"];

    const clickComponentBox = () => {
        // DESCRIBE: query parameter를 추가&수정할 때 serverProps때문에 query붙는 시간이 오래걸려서 Props를 안받아오는 방식으로 구현
        router.push(
            {query:{
                ...router.query,
                depth:component_depth,
                depthId:data.id,
            }}, 
            undefined, 
            { shallow: true });

    }
    
    return(
        <div className={classes.componentDepthBox} 
            onClick={()=>{
                clickComponentBox();
            }}
        >
            <Text 
                title={data.name} 
                type="header4"
                weight="bold"
                color="darkHigh"
                cursor="pointer"
            />
            <div className={classes.operation_title} >
                <Text
                    title={current_depth === "equipment"
                        ? "가동률"
                        : "평균 가동률"
                    }
                    type="body1"
                    color="darkMedium"
                    cursor="pointer"

                />
            </div>
            <div className={classes.operation_value} >
                <span className={classes.value} >{runInfo.runUp}</span>
                <span className={classes.percent} >%</span>
            </div>
            <div className={classes.stick_chart}>
                <StickChart
                    data={runInfoPercent}
                    color={doughnutColor}
                />
            </div>
        </div>
    )
}

export default ComponentDepthBox; 