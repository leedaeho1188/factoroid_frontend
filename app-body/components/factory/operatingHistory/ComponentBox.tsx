import React, { useEffect } from "react";
import { useSelector } from "react-redux";

import Text from "../../commons/elements/Text";
import { RootState } from "../../../store/modules";
import ComponentDepthBox from "./ComponentDepthBox";
import classes from './operatingHistory.module.scss';
import ComponentDepthFilter from "./ComponentDepthFilter";

const ComponentBox = () => {
    const data = useSelector(({operatingHistory}: RootState) => operatingHistory.data);
    const selected_data = useSelector(({operatingHistory}: RootState) => operatingHistory.selected_data);
    const current_depth = useSelector(({operatingHistory}: RootState) => operatingHistory.current_depth);
    const equipmentSize = useSelector(({operatingHistory}: RootState) => operatingHistory.equipmentSize);
    const component_depth = useSelector(({operatingHistory}:RootState) => operatingHistory.component_depth);
    const dataIndex =  data?.findIndex((f: any[]) => f[0] === component_depth);
    const component_dataList = data[dataIndex][1];

    return(
        <div className={classes.componentBox_container} >
            <Text
                title={selected_data.name + " 구성 요소"}
                type="header3"
                color="darkHigh"
                weight="bold"
            />
            <ComponentDepthFilter 
                component_depth={component_depth}
                current_depth={current_depth} 
            />
            <div className={classes.componentDepthBox_layout} >
                {component_dataList?.map((d: any, idx: any)=> {
                    if(d.upperDepth[current_depth].id == selected_data.id){
                        return  (
                            <ComponentDepthBox key={`componentDepthBox_${idx}`} data={d} component_depth={component_depth} current_depth={current_depth} equipmentSize={equipmentSize} />
                        )
                    }
                })}
            </div>
        </div>
    )
}

export default ComponentBox;