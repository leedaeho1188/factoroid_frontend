import React from "react";
import classNames from "classnames";
import { useDispatch } from "react-redux";

import classes from "./operatingHistory.module.scss";
import { ComponentDepthFilterProps } from "../../../types/yulkok/operatingHistory-types";
import { setComponentDepth } from "../../../store/modules/operatingHistory";

/**
 * 
 * @param component_depth 가동히스토리 페이지 뎁스
 * @param current_depth 구성 요소 뎁스
 */

const ComponentDepthFilter = ({
    component_depth,
    current_depth
}:ComponentDepthFilterProps) => {

    const dispatch = useDispatch();

    const handleComponentDepth = (depth:string) => {
        dispatch(setComponentDepth(depth));
    }

    return(
        <div className={classes.componentDepthFilter_box} >
            <div className={classes.filterTitle}>
                보기 단위
            </div>
            <div className={classes.componentDepthFilter_layout} >
                <div className={classNames(classes.depthButton, 
                                component_depth === "factory" && classes.selected,
                                ["company"].includes(current_depth) || classes.display_none        
                    )}
                    onClick={()=>{
                        component_depth === "factory"
                        || handleComponentDepth("factory");
                    }}
                    >공장
                </div>
                <div className={classNames(classes.depthButton, 
                                component_depth === "process" && classes.selected,
                                ["company", "factory"].includes(current_depth) || classes.display_none
                    )}
                    onClick={()=>{
                        component_depth === "process"
                        || handleComponentDepth("process");
                    }}
                    >팀
                </div>
                {/* <div className={classNames(classes.depthButton, 
                                component_depth === "depth1" && classes.selected,
                                ["company", "factory", "process"].includes(current_depth) || classes.display_none           
                    )}
                    onClick={()=>{
                        component_depth === "depth1"
                        || handleComponentDepth("depth1");
                    }}
                    >파트
                </div> */}
                <div className={classNames(classes.depthButton, 
                                component_depth === "equipment" && classes.selected,
                                ["company", "factory", "process", "depth1"].includes(current_depth) || classes.display_none
                    )}
                    onClick={()=>{
                        component_depth === "equipment"
                        || handleComponentDepth("equipment");
                    }}
                    >장비
                </div>
            </div>
        </div>
    )
}


export default ComponentDepthFilter