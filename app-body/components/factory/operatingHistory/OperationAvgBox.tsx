import React, { useEffect } from "react";
import classNames from "classnames";

import Text from "../../commons/elements/Text";
import classes from './operatingHistory.module.scss'
import DoughnutChart from "../../charts/DoughnutChart";
import { OperationAvgBoxProps, runInfoTypes, selected_dataTypes } from "../../../types/yulkok/operatingHistory-types";

/**
 * 
 * @param selected_data 선택된 data
 * @param equipmentSize 크기별 설비 그룹
 * @param current_depth 가동히스토리 페이지 뎁스
 */

const OperationAvgBox = ({
    selected_data,
    equipmentSize,
    current_depth
}:OperationAvgBoxProps) => {

    const runInfo = current_depth === "equipment" ? selected_data.infoBySize['all'].runInfo : selected_data.infoBySize[equipmentSize].runInfo;
    
    const runInfoCategory = [
                                {eng:'runUp', kor:'가동'},
                                {eng:'idle', kor:'비가동'},
                                {eng:'runDown', kor:'비가동'},
                                {eng:'off', kor:'정지'},
                                {eng:'exceptionRunUp', kor:'예외가동'}
                            ]
 
    const runInfoPercent = [
        runInfo.runUp !== '-' ? runInfo.runUp : null, 
        runInfo.idle !== '-' ? runInfo.idle : null,
        runInfo.runDown !== '-' ? runInfo.runDown : null,
        runInfo.off !== '-' ? runInfo.off : null, 
        runInfo.exceptionRunUp !== '-' ? runInfo.exceptionRunUp : null
    ]
    
    const doughnutColor = ['#116d38', '#ffc600', '#ff9f8b', '#f25341', "#dfdfe0"];
    
    return(
        <div className={classes.operationAvgBox_container} >
            <div className={classes.header} >
                <Text
                    title={current_depth === "equipment" 
                        ? "가동 실적"
                        : "평균 가동 실적"
                    }
                    type="header4"
                    weight="bold"
                />
            </div>
            <div className={classes.body} >
                <div className={classes.doughnutChartBox} >
                    <div className={classes.doughnutChartOuterBox} >
                        <DoughnutChart color={doughnutColor} data={runInfoPercent} />
                    </div>
                    <div className={classes.doughnutChartInnerBox} ></div>
                    <div className={classes.inner_info_box} >
                        <div className={classes.info_title} >
                            {current_depth === "equipment"
                                ? "가동률"
                                : "평균 가동률"
                            }
                        </div>
                        <div className={classes.info_value} >
                            <span>{runInfo.runUp}</span>
                            <span className={classes.percent} >%</span>
                        </div>
                    </div>
                </div>
                <div className={classes.categoryBox} >
                    {runInfoCategory.map((category,idx) => {
                        return(
                            <div key={`runInfoCategory-${idx}`} className={classes.category} >
                                <div className={classes.category_title} >
                                    <div className={classNames(classes.circle, classes[category.eng])} ></div>
                                    {category.kor}
                                </div>
                                <div className={classes.category_value} >
                                    <div className={classes.percent} >{runInfo[category.eng] + "%"}</div>
                                    <div className={classes.time} >
                                        {current_depth === "equipment" 
                                            ?`(${runInfo[category.eng+'Second']})`
                                            :''
                                        }
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}




export default OperationAvgBox;