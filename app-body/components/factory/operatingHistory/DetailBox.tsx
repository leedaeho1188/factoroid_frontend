import React from "react";
import { useSelector } from "react-redux";

import classes from "./operatingHistory.module.scss"
import Text from "../../commons/elements/Text";
import OperationAvgBox from "./OperationAvgBox";
import EquipOperationBox from "./EquipOperationBox";
import EquipProductionBox from "./EquipProductionBox";
import { RootState } from "../../../store/modules";


const DetailBox = () => {
    const current_depth = useSelector(({operatingHistory}: RootState) => operatingHistory.current_depth);
    const selected_data = useSelector(({operatingHistory}: RootState) => operatingHistory.selected_data);
    const equipmentSize = useSelector(({operatingHistory}: RootState) => operatingHistory.equipmentSize);

    const depthTitle = () =>{
        let depth_title;
        switch (current_depth) {
            case "company": return depth_title = "회사"; 
            case "factory": return depth_title = "공장"; 
            case "process": return depth_title = "팀"; 
            case "depth1":  return depth_title = "파트"; 
            case "equipment": return depth_title = "장비"; 
        }
        return depth_title;
    }


    return(
        <div className={classes.detailBox_container} >
            <Text 
                title={depthTitle() + " 상세 조회"}
                type="header3"
                color="darkHigh"
                weight="bold"
            />
            <div className={classes.detailBox_layout} >
                <OperationAvgBox
                    selected_data={selected_data}
                    equipmentSize={equipmentSize}
                    current_depth={current_depth}
                />
                {/* <EquipOperationBox
                    selected_data={selected_data}
                    equipmentSize={equipmentSize}
                />
                <EquipProductionBox
                    selected_data={selected_data}
                    equipmentSize={equipmentSize}
                /> */}
            </div>
        </div>
    )

}

export default DetailBox;