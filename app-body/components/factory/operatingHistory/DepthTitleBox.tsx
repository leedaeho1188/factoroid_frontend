import React, {useState} from 'react';
import classNames from 'classnames';
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router';


import classes from './operatingHistory.module.scss';
import { RootState } from "../../../store/modules";

const DepthTitleBox = () => {
    const router = useRouter();
    const data = useSelector(({operatingHistory}: RootState) => operatingHistory.data)
    const selected_data = useSelector(({operatingHistory}: RootState) => operatingHistory.selected_data);
    const current_depth = useSelector(({operatingHistory}: RootState) => operatingHistory.current_depth);
    let upper_depth = selected_data?.upperDepth;
    let upperDepth_list:any = [];
    

    if(current_depth !== "company"){
        upper_depth = Object.entries(upper_depth);

        const companyIndex = upper_depth.findIndex((f: string[]) => f[0] === "company");
        const factoryIndex = upper_depth.findIndex((f: string[]) => f[0] === "factory");
        const processIndex = upper_depth.findIndex((f: string[]) => f[0] === "process");
        const depth1Index = upper_depth.findIndex((f: string[]) => f[0] === "depth1");

        const indexList = [depth1Index, processIndex, factoryIndex, companyIndex];

        indexList.map((index) => {
            if(index !== -1){
                upperDepth_list = [upper_depth[index], ...upperDepth_list];
            }
        })
    }

    const clickDepthTitle = (depth:string, id:number) => {

        const depthIndex = data.findIndex((f: string[]) => f[0] === depth);
        const dataIndex = data[depthIndex][1].findIndex((f: { id: number; }) => f.id === id);
        const next_selectedData = data[depthIndex][1][dataIndex];

        console.log(depth, next_selectedData);

        router.push(
            {
                query:{
                    ...router.query,
                    depth: depth,
                    depthId: next_selectedData.id
                }
            },
            undefined,
            { shallow: true }
        )

    }

    return(
        <div className={classes.depthTitleBox} >
            {upperDepth_list?.map((depth:any, idx:any)=> {
                return (
                    <>
                        <div className={classes.depthTitle}
                            onClick={()=>{clickDepthTitle(depth[0], depth[1].id)}}
                        >{depth[1].name}</div>
                        <div className={classes.depthArrow} >{`>`}</div>
                    </>
                    )
                })}
            <div className={classNames(classes.depthTitle, 
                            classes.selected)} 
            >{selected_data.name}</div>
        </div>
    )

}

export default DepthTitleBox;