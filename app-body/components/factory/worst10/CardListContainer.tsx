import React from 'react'
import {
  Worst10Type,
} from "../../../types/yulkok/worst10-types";
import CardList from "./CardList";
import LeftCard from "./LeftCard";
import RightCard from "./RightCard";
import { useSelector } from "react-redux"; 


export default function CardListContainer() {
  console.log("[CardListContainer] renders");

  const worst10Data = useSelector(({ worst10 }: Worst10Type) => worst10);
  const equipments=worst10Data.data;
  const status = worst10Data.status;

  return (
    <div>
      {status == "loading" ? (
        <div>로딩중</div>
      ) : (
        <div style={{ display: "flex" }}>
          <CardList title="가동률 Worst 10" selectButtonShow={true}>
            {equipments &&
              equipments.map((equipment, i) => {
                console.log();
                return (
                  <LeftCard
                    key={`lefcard-${i}`}
                    data={equipment}
                    rank={i + 1}
                  />
                );
              })}
          </CardList>
          <CardList title="최초 동작 Worst10" selectButtonShow={false}>
            {equipments &&
              equipments.map((equipment, i) => {
                return (
                  <RightCard
                    key={`rightcard-${i}`}
                    data={equipment}
                    rank={i + 1}
                  />
                );
              })}
          </CardList>
        </div>
      )}
    </div>
  );
}
