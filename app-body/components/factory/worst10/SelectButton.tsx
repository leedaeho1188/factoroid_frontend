import classes from './worst10.module.scss';
import {useRef, useEffect} from 'react';
import { useSelector, useDispatch } from "react-redux"; 
import * as selectedActions from '../../../store/modules/selected'
import classNames from "classnames";
import {SelectButtonProps, SelectedType} from "../../../types/yulkok/worst10-types";

function SelectButton({selectButtonShow}:SelectButtonProps){
    console.log("[SelectButton] renders");
    
    const SelectButtonContainer=useRef<HTMLDivElement>(null);
    const dispatch = useDispatch();
    const item = useSelector(
      ({selected}:SelectedType)=>selected.data
    )

    const handleButton = (e: React.MouseEvent<HTMLElement>) : void => {
      const target = e.target as HTMLButtonElement
      if(target.name){
        switch(target.name){
          case 'loss': return clickLoss();
          case 'idle': return clickIdle();
          case 'off' : return clickOff();
          default: return 
        }
      }
    };

    useEffect(() => {
      if (selectButtonShow == false && SelectButtonContainer.current) {
        SelectButtonContainer.current.style.display = "none";
      }
    }, []);

    const clickLoss =()=>{
      dispatch(selectedActions.clickLoss());
    }
    const clickIdle = () => {
      dispatch(selectedActions.clickIdle());
    };

    const clickOff = () => {
      dispatch(selectedActions.clickOff());
    };

    return (
      <div ref={SelectButtonContainer}>
        <div className={classes.select_title}>선정 기준</div>
        <ul className={classes.select_list}>
          <li className={classes.select_item}>
            <button
              name="loss"
              onClick={handleButton}
              className={classNames(
                classes.select_btn,
                item.loss ? classes.selected : ""
              )}
              data-testid="Loss버튼"
            >
              Loss(비가동+유휴)
            </button>
          </li>
          <li className={classes.select_item}>
            <button
              name="idle"
              onClick={handleButton}
              className={classNames(
                classes.select_btn,
                item.idle ? classes.selected : ""
              )}
              data-testid="유휴버튼"
            >
              유휴
            </button>
          </li>
          <li className={classes.select_item}>
            <button
              name="off"
              onClick={handleButton}
              className={classNames(
                classes.select_btn,
                item.off ? classes.selected : ""
              )}
              data-testid="비가동버튼"
            >
              비가동
            </button>
          </li>
        </ul>
      </div>
    );
}
export default SelectButton;