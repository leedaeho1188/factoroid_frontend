import React ,{ useEffect, useState } from 'react';
import {
  Worst10ObjectProps,
  SelectedType,
} from "../../../types/yulkok/worst10-types";
import LineChart from '../../charts/LineChart';
import StickChart from '../../charts/StickChart';
import { useSelector } from "react-redux"; 
import classes from './worst10.module.scss';


// DESCRIBE : 가동률 Worst 10 
function LeftCard({ data, rank }: Worst10ObjectProps) {
  const [result, setResult]=useState(data);
  const item = useSelector(({ selected }: SelectedType) => selected.data);

  useEffect(()=>{
    handleSelectedData();
  },[item])

  const handleSelectedData=()=>{
    try{
      if(item.loss){
        return CardBody("Loss율", result.lossInfo.rate, result.lossInfo.trend);
      }
      else if(item.idle){
        return CardBody("유휴율", result.idleInfo.rate, result.idleInfo.trend);
      }
      else{
        return CardBody("비가동율", result.offInfo.rate, result.offInfo.trend);
      }
    }
    catch{
      return
    }
  }

  const CardBody=(title:string, rate:string, trendList: number[])=>{
     return (
       <>
         <div className={classes.card_body}>
           <div className={classes.card_body_left}>
             <div className={classes.card_body_title}>{title}</div>
             <div className={classes.card_body_ratio}>
               <span className={classes.card_body_highlight}>
                 {parseFloat(rate).toFixed(2)}
               </span>
               %
             </div>
             <StickChart data={trendList} key={`stickchart-${rank}`} />
           </div>
           <div className={classes.card_body_right}>
             <LineChart data={trendList} />
           </div>
         </div>
       </>
     );
  }

  return (
    <div className={classes.card}>
      <div className={classes.card_header}>
        <div className={classes.card_rank}>{rank}</div>
        <div className={classes.card_title}>{result.name}</div>
        <div>{result.activeTime}</div>
      </div>
      {handleSelectedData()}
    </div>
  );
}

export default React.memo(LeftCard); 