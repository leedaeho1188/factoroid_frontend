import {
  Worst10Props,
} from "../../../types/yulkok/worst10-types";
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux"; 

import Text from "../../commons/elements/Text";
import Layout from "../../commons/EntireLayout/Layout";
import InquiryLayout from "../../commons/InquiryLayout/InquiryLayout";
import { DateType } from "../../../types/common-types";
import {formatDate} from "../../../shared/function";
import CardListContainer from "./CardListContainer";
import { ThunkDispatchType } from "../../../store/modules/index";
import { worst10Thunk } from "../../../store/modules/worst10";
import * as Sentry from "@sentry/browser";
import * as worst10Actions from "../../../store/modules/worst10";
import useInquiryLayout from "../../../hooks/useInquiryLayout";

// DESCRIBE : worst10 메인 페이지
function Worst10({equipments}: Worst10Props) {
  const [equipType, date, workingHour, setDate, setWorkingHour] = useInquiryLayout();

  // // worst10 비동기 데이터 갱신을 위한 distpach
  const dispatch = useDispatch<ThunkDispatchType>();

  // worst10 비동기 데이터 초기화 
  useEffect(()=>{
    dispatch(worst10Actions.initializeData(equipments));
  })
  // 날짜 바뀌면, 데이터 재요청 및 상태 업데이트
  useEffect(() => {
    requestData(date, workingHour);
  }, [date,workingHour]);
  

  const requestData = async (date: DateType, workingHour: string) => {
    await dispatch(
      worst10Thunk({
        startDate: formatDate(date.startDate),
        endDate: formatDate(date.endDate),
        workingHour: workingHour,
      })
    );
  };


  return (
    <Layout>
      <div>
        <Text
          title="Worst 10"
          type="header1"
          color="primary500"
          weight="bold"
        ></Text>
        <InquiryLayout
          timePicker
          workingHour
          // equipmentType
          setWorkingHour={setWorkingHour}
          setDate={setDate}
          date={date}
        />
      </div>
      <button>API 테스트 버튼</button>
      <CardListContainer ></CardListContainer>
    </Layout>
  );
}

export default React.memo(Worst10);
