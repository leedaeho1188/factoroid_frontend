import classes from './worst10.module.scss';
import { Worst10ObjectProps } from "../../../types/yulkok/worst10-types";

// DESCRIBE : 최초 동작 Worst 10 
function RightCard({ data ,rank }: Worst10ObjectProps) {
  return(
    <div className={classes.card}>
        <div>{rank}</div>
        <div>{data.name}</div>
    </div>
  );
}

export default RightCard; 