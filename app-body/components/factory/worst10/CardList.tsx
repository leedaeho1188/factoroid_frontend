import classes from './worst10.module.scss';
import SelectButton from'./SelectButton';
import {CardListProps} from "../../../types/yulkok/worst10-types";

function CardList ({children, title, selectButtonShow}:CardListProps){
    return (
      <div className={classes.cardList}>
        <div className={classes.cardList_header}>
          <div>{title}</div>
          <SelectButton selectButtonShow={selectButtonShow} />
        </div>
        {children}
      </div>
    );
}

export default CardList; 