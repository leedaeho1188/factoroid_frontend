import {useEffect, useState} from 'react';
import classes from './chart.module.scss';

interface ChartProps {
  data: Array<any>,
  color: Array<string>,
}

interface StickBarProps{
  index:number
}

StickChart.defaultProps = {
  data: [10, 30, 20],
  color: ["rgb(255, 198, 0)", "rgb(255, 159, 139)", "rgb(180, 139, 255)"],
};

/**
 * 
 * @param data: 차트에 들어가는 데이터 리스트
 * @param color: 차트에 사용되는 컬러 리스트
 * @returns 
 */
function StickChart({
    // data : 차트에 들어가는 데이터 리스트
    data, 
    // color : 차트에 사용되는 컬러 리스트
    color
  }: ChartProps) {
  const [dataWidth, setDataWidth]=useState<Array<number>>([]);

  const handleChartRate=()=>{
    // console.log(data);
    let data_width_list = new Array<any>();
    data_width_list.push(parseFloat(data[0]));
    for(let i=1; i<data.length; i++){
      data_width_list[i]=parseFloat(data_width_list[i-1])+parseFloat(data[i]);
    }
    setDataWidth(data_width_list);
  }

  useEffect(() => {
    handleChartRate();
  }, [data]);

  const StickBar = ({ index }: StickBarProps) => {
    return (
      <div>
        <div
          className={classes.data_line}
          style={{
            width: `${dataWidth[index]}%`,
            backgroundColor: color[index],
            zIndex: 500 - index,
          }}
        ></div>
      </div>
    );
  };
  return (
    <div className={classes.base_line}>
      {data.map((data, index) => {
        // console.log(data);
        // console.log(dataWidth[index]);
        // console.log(color[index]);
        return (
          // eslint-disable-next-line react/jsx-key
          <div key={index}>
            <div
              className={classes.data_line}
              style={{
                width: `${dataWidth[index]}%`,
                backgroundColor: color[index],
                zIndex: 500 - index,
              }}
            ></div>
          </div>
        );
      })}
    </div>
  );
}

export default StickChart;