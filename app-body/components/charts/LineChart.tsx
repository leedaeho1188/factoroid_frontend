import ReactEcharts from "echarts-for-react";
import {useState, useEffect} from 'react';
import {dataProps} from './types';


function LineChart({data}:dataProps) {
  const [label, setLabel]=useState<Array<number>>([]);

  const makeLabel=()=>{
    for (let i=0; i<data.length ;i++){
      setLabel(label=>[...label,i]);
    }
  }
  useEffect (()=>{
    makeLabel();
  },[]);
  
  const option = {
    backgroundColor: "#fff",
    tooltip: {
      trigger: "axis",
      position: function (pt: any) {
        return [pt[0], "10%"];
      },
    },
    // toolbox: {
    //   feature: {
    //     dataZoom: {
    //       yAxisIndex: "none",
    //     },
    //     restore: {},
    //     saveAsImage: {},
    //   },
    // },
    // dataZoom: [
    //   {
    //     type: "slider",
    //     start: 0,
    //     end: 10,
    //     height: 10,
    //     bottom: 20,
    //   }
    // ],
    // title: {
    //   left: "center",
    //   text: "라인 차트",
    // },
    xAxis: {
      type: "category",
      splitLine: false,
      data: label,
    },
    yAxis: {
      type: "value",
      splitLine: false,
      max: 100,
    },
    series: [
      {
        data: data,
        type: "line",
        lineStyle: {
          color: "#006432",
        },
        symbol: "circle",
        symbolSize: "5",
        color: "#006432",
      },
    ],
  };
  return (
    <ReactEcharts option={option} style={{ height: "100px", width: "100%" }} />
  );
}

export default LineChart;
