export interface Data {
  checked: boolean;
  depth: string;
  display: string;
  lowerDepth: number[];
  name: string;
  state_id: number;
  upperDepth: number[];
  total_checked_num: number;
  unchecked_num: number;
  checked_num: number;
  section_display: string;
}

export interface DepthInfoData {
  depthInfo: {
    data: Data[];
  };
}

export interface Content {
  state_id: number;
  id: number;
  name: string;
  factoryList: Array<any>;
  processList: Array<any>;
  depth1List: Array<any>;
  lowerDepth: Array<any>;
  upperDepth: upperDepth;
}

export interface upperDepth{
  id:number,
  name:string,
  processList: Array<number>,
  lowerDepth:Array<number>,
  state_id:number,
}

export interface DepthSectionProps {
  depth: string;
  content: Content;
}

export interface DepthSectionProps {
  depth: string;
}

export interface DepthCheckBoxProps {
  depth: string;
  content: any;
}

export interface CheckedInfoItem {
  state_id: number;
  total_checked: number;
  checked: number;
  unchecked: number;
  depth: string;
  display: string;
  name: string;
}
export interface CheckedInfo {
  checkedInfo: {
    data: Array<CheckedInfoItem>;
  };
}
