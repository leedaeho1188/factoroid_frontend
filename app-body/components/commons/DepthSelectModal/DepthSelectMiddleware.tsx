import React, {useEffect, useRef, useState} from 'react';
import DepthSection from './DepthSection';
import {DepthSectionProps} from './depth-info-types'
import { useSelector, useDispatch } from "react-redux"; 
import * as depthInfoActions from "../../../store/modules/depthInfo";



export default function DepthSelectMiddleware({ depth, content }: DepthSectionProps) {
    const dispatch = useDispatch();
    const [initial, setInitial]=useState(true);

    useEffect(() => {
      initializeDisplayState();
    }, []);

    const initializeDisplayState =()=>{
        if (initial) {
          if (content.lowerDepth !== undefined) {
            const length =
              content.lowerDepth.length == undefined
                ? 0
                : content.lowerDepth.length;

            dispatch(
              depthInfoActions.addCheckedInfo({
                state_id:
                  content.state_id == undefined ? 999 : content.state_id,
                total_checked_num: length,
                checked_num: length,
                unchecked_num: 0,
                section_display: "block",
              })
            );
          }
          setInitial(false);
        }

    }

    return (
        <div>
            {!initial &&
                <DepthSection
                key={`depth-section-factory`}
                depth={depth}
                content={content}
                ></DepthSection>
            }
        </div>
    );
}
