import React, {useState, useEffect, useCallback} from 'react';
import { depthData } from "../../../types/common/depth-types";
import classes from './depth-modal.module.scss';
import Button from '../elements/buttons/Button';
import DepthSelectMiddleware from "./DepthSelectMiddleware";
import {useDispatch } from "react-redux"; 
import * as depthInfoActions from "../../../store/modules/depthInfo";

/**
 * 
 * @param id : 공장 아이디
 * @param factory :공장 이름
 * @param data : 공장 뎁스 데이터 
 * @returns 뎁스 정보를 가지고 와서 필터링 할 수 있는 모달 컴포넌트
 */
function DepthSelectModal({ id, factory, data }: depthData) {
    const [isOpen, setIsOpen]=useState(false);
    const [firstDepth, setFirstDepth] = useState<any[]>([]);
    const [secondDepth, setSecondDepth]=useState<any[]>([]);
    const [thirdDepth, setThirdDepth] = useState<any[]>([]);
    const label=[['factory','process','part'],['공장','팀','파트']];

    const dispatch = useDispatch();

    
    useEffect(() => {
      makeDepthData();
    }, []);

    const makeDepthData = useCallback(() => {
      parsedDepthData();
      initializeDepthState();
    }, []);

    const onClickOpenButton = () => {
      setIsOpen(true);
    };
    const onClickCloseButton = () => {
      setIsOpen(false);
    };    


    const parsedDepthData=()=>{
      let state_id = 0;
      
      for (let i = 0; i < data.length; i++) {
        let factory = data[i];
        console.log(factory);
        data[i].lowerDepth = data[i].processList;
        data[i].state_id = state_id;
        state_id += 1;
        for (let j = 0; j < factory.processList.length; j++) {
          let process = factory.processList[j];
          data[i].processList[j].upperDepth = factory;
          data[i].processList[j].lowerDepth = data[i].processList[j].depth1List;
          data[i].processList[j].state_id = state_id;
          state_id += 1;
          for (let s = 0; s < process.depth1List.length; s++) {
            data[i].processList[j].depth1List[s].upperDepth = process;
            data[i].processList[j].depth1List[s].state_id = state_id;
            state_id += 1;
          }
        }
      }
      // DESCRIBE : 첫번째, 두번째, 세번째 뎁스 데이터 만들기 
      const tmp = {
        id: id,
        name: factory,
        factoryList: data,
        state_id: 999,
        lowerDepth: data,
      };
      setFirstDepth((prev) => [...prev, tmp]);
      for (let i = 0; i < data.length; i++) {
        setSecondDepth((prev) => [...prev, data[i]]);
        for (let j = 0; j < data[i].processList.length; j++) {
          setThirdDepth((prev) => [...prev, data[i].processList[j]]);
        }
      }
    }

    // DESCRIBE : 리덕스 상태값 초기화 (chekecd -> 체크 박스 상태값)
    const initializeDepthState =()=>{
          handleDispatch(
            factory,
            "total",
            true,
            "block",
            data.map((x)=>x.state_id),
            [],
            999
          );
          for (let i = 0; i < data.length; i++) {
            let factory = data[i];
            let lower_depth = factory.lowerDepth?.map((x) => x.state_id);
            handleDispatch(
              factory.name,
              "factory",
              true,
              "block",
              lower_depth,
              [999],
              factory.state_id
            );
            for (let j = 0; j < factory.processList.length; j++) {
              let process = factory.processList[j];
              let lower_depth = process.lowerDepth?.map((x) => x.state_id);
              let upper_depth = [process.upperDepth?.state_id];
               handleDispatch(
                 process.name,
                 "process",
                 true,
                 "block",
                 lower_depth,
                 upper_depth,
                 process.state_id
               );
              for (let s = 0; s < process.depth1List.length; s++) {
                let depth1 = process.depth1List[s];
                let upper_depth = [depth1.upperDepth?.state_id];
                handleDispatch(
                  depth1.name,
                  "part",
                  true,
                  "block",
                  [],
                  upper_depth,
                  depth1.state_id
                );
              }
            }
          }
    }
    const handleDispatch = (
      name: string,
      depth: string,
      checked: boolean,
      display: string,
      lowerDepth?: any,
      upperDepth?: any,
      state_id?: number
    ) => {
      dispatch(
        depthInfoActions.addDepthInfo({
          state_id: state_id,
          name: name,
          upperDepth: upperDepth,
          lowerDepth: lowerDepth,
          depth: depth,
          checked: checked,
          display: display,
        })
      );
    }; 

    const handleConfirm =()=>{
      setIsOpen(false);
    }
    const DepthSelectHeader=()=>{
      return(
       <div className={classes.header}>
         <div className={classes.title}>조회 범위 지정</div>
         <div className={classes.close_btn} onClick={onClickCloseButton}>
           X{/* <img src="images/btn_32_px_delete.svg" /> */}
         </div>
       </div>
      )
    }
    const DepthSelectCol = (data: any, index: number) => {
      return(
        <div className={classes.col}>
          <div className={classes.subtitle}>{label[1][index]}</div>
          <div className={classes.section}>
            {data &&
              data.map((item: any, i: number) => {
                return (
                  <DepthSelectMiddleware
                    key={`depth-section-part-${label[0][index]}`}
                    depth={label[0][index]}
                    content={item}
                  ></DepthSelectMiddleware>
                );
              })}
          </div>
        </div>
      )
    };

    const DepthSelectBody=()=>{
      return(
        <div className={classes.body}>
          {DepthSelectCol(firstDepth, 0)}
          {DepthSelectCol(secondDepth, 1)}
          {DepthSelectCol(thirdDepth, 2)}
        </div>
      )
    }

    const DepthSelectFooter=()=>{
      return (
        <div className={classes.footer}>
          <div className={classes.footer_section}>
            <div className={classes.tag_box}>
              <div className={classes.factory_tag_section}></div>
              <div className={classes.process_tag_section}></div>
              <div className={classes.depth1_tag_section}></div>
            </div>
          </div>
          <div className={classes.footer_section}>
            <div className={classes.confirm_btn}>
              <Button
                size="large"
                text="조회 범위 지정하기"
                style="line"
                onClick={handleConfirm}
              ></Button>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div>
        {!isOpen ? (
          <div className={classes.temp}>
            <Button
              size="large"
              color="primary"
              text="조회 범위 지정하기"
              style="solid"
              onClick={onClickOpenButton}
            ></Button>
          </div>
        ) : (
          <div className={classes.depth_select_modal}>
            {DepthSelectHeader()}
            {DepthSelectBody()}
            {DepthSelectFooter()}
          </div>
        )}
      </div>
    );
}


export default React.memo(DepthSelectModal);