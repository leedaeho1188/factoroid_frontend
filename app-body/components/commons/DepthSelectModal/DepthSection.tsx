import React, { useState, useEffect } from 'react'
import {
  DepthInfoData,
  DepthSectionProps,
} from "./depth-info-types";
import classes from './depth-modal.module.scss';
import DepthCheckbox from './DepthCheckbox';
import * as depthInfoActions from "../../../store/modules/depthInfo";
import { useSelector, useDispatch } from "react-redux"; 

function DepthSection({ depth, content }: DepthSectionProps) {
  const [checked, setChecked] = useState(0);
  const dispatch = useDispatch();
  const item = useSelector(
    ({ depthInfo }: DepthInfoData) => depthInfo.data
  )[content.state_id];


  useEffect(() => {
    // DESCRIBE  해당 섹션이 다 비엇을 떄,
    if (item.checked_num == 0 && item.display == "none") {
      if (content.upperDepth && depth == "process") {
        handleFactoryCheckBox(content.upperDepth.state_id);
      }
    }
  }, [item]);


  const handleFactoryCheckBox = (state_id:number) => {
    dispatch(
      depthInfoActions.handleUpperChecked({
        id: state_id,
        lower_id: content.state_id,
        checked: false,
      })
    );
  };

  const handleMinusChecked =()=>{
    dispatch(
      depthInfoActions.minusCheckedInfo({
        upper_id: content.upperDepth.state_id,
      })
    );
  }

  const handleCheckBox=()=>{
    switch(depth){
      case "factory": return renderFactory()
      case "process": return renderProcess()
      case "part" : return renderPart()
    }
  }

  const renderFactory=()=>{
    return(
       content.factoryList &&
          content.factoryList.map((lower, i) => {
            return (
              <DepthCheckbox
                key={`checkbox-factory-${i}`}
                depth="factory"
                content={lower}
              ></DepthCheckbox>
            );
        })
    )
  }

  const renderProcess=()=>{
    return(
      content.processList &&
          content.processList.map((lower, i) => {
            return (
              <DepthCheckbox
                key={`checkbox-process-${i}`}
                depth="process"
                content={lower}
              ></DepthCheckbox>
            );
      })
    )
  }
  const renderPart=()=>{
    return (
       content.depth1List &&
          content.depth1List.map((lower, i) => {
            return (
              <DepthCheckbox
                key={`checkbox-part-${i}`}
                depth="part"
                content={lower}
              ></DepthCheckbox>
            );
        })
    );
  }
  // TODO: content 에서 하위 뎁스 리스트 뺴오고, lowerDepth 에 저장.
  return (
    <div
      id={`${depth}-${content.state_id}-select-container`}
      style={{ display: item.display }}
      >
      <div className={classes.upper_depth_name}>
        {depth==="process"?`${content.name} 공장`: `${content.name}`}
      </div>
      <ul
        className={classes.checkbox_container}
        id={`${depth}-${content.state_id}-fragment`}
      >
        {handleCheckBox()}
      </ul>
    </div>
  );
}
export default React.memo(DepthSection);