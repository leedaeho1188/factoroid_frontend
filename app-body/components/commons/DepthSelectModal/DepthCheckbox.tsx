import React, {useState, useEffect, useCallback} from 'react';
import { useSelector, useDispatch } from "react-redux"; 
import {
  DepthCheckBoxProps,
  DepthInfoData,
  CheckedInfo,
} from "./depth-info-types";
import * as depthInfoActions from '../../../store/modules/depthInfo';
import classes from "./depth-modal.module.scss";


function DepthCheckbox({depth, content}:DepthCheckBoxProps) {
  const [isChecked, setIsChecked] = useState(true);
  const [depthType, setDepthType] = useState<string>("");
  const [id, setId] = useState<number>();
  const [upperDepth, setUpperDepth] = useState<number>();
  const [lowerDepth, setLowerDepth] = useState<number[]>();

  // DESCRIBE : 체크 박스 컴포넌트 상태값 불러오기
  const dispatch = useDispatch();
  const item = useSelector(({ depthInfo }: DepthInfoData) => depthInfo.data)[
    content.state_id
  ];

  // DESCRIBE : 상태 값 변경될 때마다 발생
  useEffect(() => {
    handleDepthInfo();
  }, [item]);

  // DESCRIBE : 체크 박스 상태값 바꼈을 때 초기화
  const handleDepthInfo = () => {
    if (item.upperDepth) {
      setUpperDepth(item.upperDepth[0]);
    }
    if (item.lowerDepth) {
      setLowerDepth(item.lowerDepth);
    }
    setDepthType(depth);
    setId(item.state_id);
    setIsChecked(item.checked);
  };

  // DESCRIBE : 체크 박스 눌렀을 때 발생하는 이벤트
  const handleCheckBox = (e: any) => {
    const checked_ = !isChecked;
    switch (depthType) {
      case "factory":
        return handleFactoryCheckBox(checked_);
      case "process":
        return handleProcessCheckBox(checked_);
      case "part":
        return handlePartCheckBox(checked_);
      default:
        return;
    }
  };

  // DESCRIBE : 공장 단위 체크 박스 눌렀을 때
  const handleFactoryCheckBox = (checked: boolean) => {
    if (checked == true) {
      dispatch(
        depthInfoActions.handleFactoryChecked({
          id: id,
          id_list: lowerDepth,
          checked: true,
        })
      );
      addCheckAction();
      handleDisplayLowerDepth();
    } else {
      dispatch(
        depthInfoActions.handleFactoryChecked({
          id: id,
          id_list: lowerDepth,
          checked: false,
        })
      );
      minusCheckAction();
    }
    getCheckNumber();
  };

  // DESCRIBE : 프로세스 단위 체크 박스 눌렀을 때
  const handleProcessCheckBox = (checked: boolean) => {
    if (checked == true) {
      dispatch(
        depthInfoActions.handleProcessChecked({
          id: id,
          checked: true,
        })
      );
      addCheckAction();
      handleDisplayLowerDepth();
    } else {
      dispatch(
        depthInfoActions.handleProcessChecked({
          id: id,
          checked: false,
        })
      );
      // TODO:
      dispatch(
        depthInfoActions.handleBaseChecked({
          id: upperDepth,
          checked: false,
        })
      );
      minusCheckAction();
    }
    getCheckNumber();
  };

  // DESCRIBE : 파트 단위 체크 박스 눌렀을 때
  const handlePartCheckBox = (checked: boolean) => {
    if (checked == true) {
      dispatch(
        depthInfoActions.handlePartChecked({
          id: id,
          checked: true,
        })
      );
      addCheckAction();
    } else {
      dispatch(
        depthInfoActions.handlePartChecked({
          id: id,
          checked: false,
        })
      );
      minusCheckAction();
    }
    getCheckNumber();
  };

  // DESCRIBE : 상위 뎁스 체크 true 일 때, 하위 뎁스 모두 체크 + 보여주기
  const handleDisplayLowerDepth = () => {
    dispatch(
      depthInfoActions.displayLowerDepth({
        this_id: content.state_id,
        upper_id:
          content.upperDepth !== undefined ? content.upperDepth.state_id : -999,
        lower_id_list: content.lowerDepth.map((item: any) => item.state_id),
      })
    );
  };

  // DESCRIBE : check 갯수 +1
  const addCheckAction = () => {
    dispatch(
      depthInfoActions.plusCheckedInfo({
        upper_id: upperDepth,
      })
    );
  };

  // DESCRIBE : check 갯수 -1
  const minusCheckAction = () => {
    dispatch(
      depthInfoActions.minusCheckedInfo({
        upper_id: upperDepth,
      })
    );
  };

  // DESCRIBE : 현재까지 얼마자 체크 됐는지 확인 후 display 변경
  const getCheckNumber = () => {
    dispatch(
      depthInfoActions.getCheckedNumber({
        upper_id: upperDepth,
      })
    );
  };

  const handleChange = () => {
    return;
  };

  return (
    <div>
      <li className={classes.checkbox_item}>
        <input
          type="checkbox"
          checked={isChecked}
          name={`${depth}-${content.state_id}-checkbox`}
          className={classes.checkbox}
          value={`${content.state_id}`}
          id={`${depth}-${content.state_id}-checkbox`}
          onChange={handleChange}
        ></input>
        <span
          className={classes.checkmark}
          id={`${depth}-${content.state_id}-customed-checkbox`}
          onClick={handleCheckBox}
        ></span>
        <label
          className={classes.checkbox_label}
          htmlFor={`${depth}-${content.state_id}-checkbox`}
          onClick={handleCheckBox}
        >
          {content.name}
        </label>
      </li>
    </div>
  );
}


export default React.memo(DepthCheckbox);