import React, {useState} from 'react';
import classNames from "classnames";

import classes from './calendar.module.scss';
import { DatesProps } from '../../../types/common-types';

/**
 * 
 * @param dateInfo date 정보 {condition,value,date,weekend}
 * @param onClickDate date 클릭시 이벤트 함수
 * @param btwDates date가 시작과 종료 날짜 사이에 있는지
 * @param startDate date가 시작 날짜인지
 * @param endDate date가 종료 날짜인지
 * @returns 
 */
const Dates = ({
    dateInfo,
    onClickDate,
    btwDates, 
    startDate,
    endDate
}:DatesProps) => {

    return(
        <div className={classNames(
                    classes.date, 
                    classes[dateInfo.condition], 
                    // 조회 시작 날짜 조회 종료 날짜 사이 날짜 && 해당 달의 날짜
                    btwDates && dateInfo.condition === 'this' ? classes.btwDates : '',
                    startDate? classes.startDate : '',
                    endDate? classes.endDate : '',
                    // 날짜가 오늘보다 미래일경우 future class 추가
                    dateInfo.value.getTime() > new Date().getTime()? classes.future: '',
                )}
            onClick={()=>{
                if(dateInfo.condition === "this" && dateInfo.value.getTime() <= new Date().getTime()){
                    onClickDate(dateInfo.value);
                }
            }}
        >   
            <div className={classes.background} ></div>
            <span className={classes[dateInfo.weekend]} >{dateInfo.date}</span>
        </div>
    )

}


export default Dates;