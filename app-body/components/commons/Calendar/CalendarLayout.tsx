import React, {useEffect, useState} from "react";

import classes from "./calendar.module.scss"
import { MultipleCalendarsProps } from "../../../types/common-types";
import Button from "../elements/buttons/Button";
import Calendar from "./Calendar";

/**
 * 
 * @param date 달력 날짜
 * @param start 조회 시작 날짜
 * @param end 조회 종료 날짜
 * @param onClickSubmitBtn 날짜 적용하기 벝는 클릭시 이벤트 함수
 * @returns 
 */

export const MultipleCalendars = ({
        date,
        start,
        end,
        onClickSubmitBtn,
    }:MultipleCalendarsProps) => {

    useEffect(()=>{
        // 이 컴포넌트가 없어질 때 return 안에 코드가 실행됨
        // 처음 달력이 렌더됬을 때로 시작 & 종료 돌아감
        return(()=> {
            setStartDate(start);
            setEndDate(end);
        })
    },[])

    // 달력 안에서 시작 일시
    const [startDate, setStartDate] = useState<Date>(start);
    // 달력 안에서 종료 일시
    const [endDate, setEndDate] = useState<Date>(end);
    // 달력 날짜
    const [currentDate, setCurrentDate] = useState<Date>(date);

    // 달력 한 달 앞으로 넘어가기
    const prevMonth = () => {
        setCurrentDate(new Date(currentDate.getFullYear(), currentDate.getMonth()-1, currentDate.getDate()))
    }
    // 달력 한 달 뒤로 넘어가기
    const nextMonth = () => {
        setCurrentDate(new Date(currentDate.getFullYear(), currentDate.getMonth()+1, currentDate.getDate()))
    }
    // Date를 클릭했을 때
    const onClickDate= (value:Date) => {
        // 클릭 한 Date가 시작 날짜보다 늦을 날짜일 때
        if(value.getTime() > startDate.getTime()){
            // 클릭 한 Date가 종료 날짜랑 같은 날짜일 때
            if(value.getTime() === endDate.getTime()){
                setStartDate(value)
            }else{
                setEndDate(value)
            }
        }else{
            setStartDate(value);
            setEndDate(value);
        }
    }
    // 날짜 적용하기 버튼 눌렀을 때 상위 컴포넌트에 적용된 날짜 전달 함수 
    const onClickSubmitDate = () => {
        onClickSubmitBtn(startDate, endDate);
    }

    return(
        <div className={classes.calendar_container} >
            <div className={classes.calendar_header} >
                <div className={classes.prevBtn} onClick={prevMonth} > 전달 </div>
                <div className={classes.nextBtn} onClick={nextMonth} > 담달 </div>
            </div>
            <div className={classes.calendar_body} >
                <Calendar date={currentDate} start={startDate} end={endDate} onClickDate={onClickDate} />
                <Calendar date={new Date(currentDate.getFullYear(), currentDate.getMonth()+1, currentDate.getDate())} start={startDate} end={endDate} onClickDate={onClickDate} />
            </div>
            <div className={classes.calendar_footer} >
                <Button 
                    onClick={onClickSubmitDate} 
                    text="날짜 적용하기" 
                    size="large"  
                    style="solid" />
            </div>
        </div>
    )
}