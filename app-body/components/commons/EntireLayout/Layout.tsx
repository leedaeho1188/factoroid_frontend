import React, { useState, ReactChild, ReactChildren } from 'react';
import classNames from 'classnames';

import Navbar from './Navbar';
import classes from './entire-layout.module.scss';


// 전체 페이지 레이아웃 컴포넌트

interface LayoutProps {
    children : ReactChild | ReactChild[] | ReactChildren | ReactChildren[] ;
}

const Layout = ({children}: LayoutProps) => {

    const [close, setClose] = useState<boolean>(false)

    return (
        <div className={classes.layoutContainer} >
            <Navbar close={close} setClose={setClose}/>
            <div className={classNames(classes.pageBody, close? classes.close : "")} >{children}</div>
        </div>
    )
}

export default Layout