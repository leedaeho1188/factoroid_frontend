import Link from 'next/link';
import React, {useState, useEffect} from 'react'
import classes from '../common.module.scss'
import classNames from 'classnames'
import { NavMenuProps } from '../../../types/common-types';
import {useRouter} from 'next/router';


// Header에있는 각각 메뉴 버튼

const NavMenu : React.FC<NavMenuProps> = ({page, key, customer}) => {
    const [selected, setSelected] = useState<boolean>(false);
    const router = useRouter();

    useEffect(() => {
        const is_selected = window.location.href.includes(page.path);
        setSelected(is_selected);
    },[])

    const handleRoute= ()=>{
        console.log('handleRoute 함수 실행');
        router.push(`/${customer}/${page.path}`);
    }

    return (
      <>
        {/* <Link key={key} href={`/${customer}/${page.path}`} passHref> */}
        <div
          className={classNames(
            `${classes.menu}`,
            `${selected ? classes.selected : ""}`
          )}
          onClick={handleRoute}
        >
          {page.title}
        </div>
        {/* </Link> */}
      </>
    );
}

export default NavMenu;