import React from "react";
import classes from "./inquiry-layout.module.scss";
import { InquiryLayoutProps } from "../../../types/common-types";
import TimePicker from "./TimePicker";
import WorkingHour from "./WorkingHour";
import EquipmentType from "./EquipmentType";

// 공통 조회 레이아웃 
// 모든 종류의 조회 컴포넌트를 조건에 따라서 렌더링할 수 있도록

/**
 * 
 * @param timePicker  조회 기간 
 * @param workingHour 근무 시간
 * @param equipmentType 크기별 설비 그룹
 * @param setEquipType 크기별 설비 설정 함수
 * @param setWorkingHour 근무 시간 설정 함수
 * @param setDate 조회 기간 설정 함수 
 * @param date 조회 기간 {startDate, endDate}
 * @returns 
 */

const InquiryLayout = ({
    timePicker=false, 
    workingHour=false, 
    equipmentType=false,
    equipmentTypeData,
    workingHourData,
    // setEquipType,
    // setWorkingHour,
    date
}:InquiryLayoutProps) => {

    console.debug('[InquiryLayout] renders')
    
    return (
        <div className={classes.inquiryLayout} >
            {timePicker &&
                <TimePicker 
                    start={date.startDate} 
                    end={date.endDate} 
                />
                    
            }
            {workingHour &&
                <>
                    <div className={classes.verticalLine} />
                    <WorkingHour 
                        workingHourData={workingHourData}
                        // setHour={setWorkingHour} 
                    /> 
                </>
            }
            {equipmentType &&
                <>
                    <div className={classes.verticalLine} />
                    <EquipmentType
                        equipmentTypeData={equipmentTypeData}
                        // setEquipType={setEquipType}
                    />
                </>
            }
        </div>
    )
}

export default InquiryLayout;