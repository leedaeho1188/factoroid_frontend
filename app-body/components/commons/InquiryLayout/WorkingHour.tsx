import classNames from "classnames";
import React, {useEffect, useState} from "react";
import { useRouter } from "next/router";

import Text from "../elements/Text";
import classes from './inquiry-layout.module.scss';
import { WorkingHourProps } from "../../../types/common-types";


// 근무 시간 컴포넌트
const WorkingHour = ({
    workingHourData
}:WorkingHourProps) => {
    const router = useRouter();

    const [workingHour, setWorkingHour] = useState<string|undefined>(workingHourData);

    useEffect(()=>{
        setWorkingHour(workingHourData)
    },[workingHourData])

    const clickWorkingHour = (hour:string) => {
        setWorkingHour(hour);

        router.push(
            {query:{
                    ...router.query,
                    workingHour: hour,
            }},
            undefined,
            {shallow:true}
        )

        // setHour(hour);
    }

    return (
        <div className={classes.inquiryBox}>
            <Text title="근무 시간" weight="bold" type="body1"/>
            <div className={classNames(classes.body, classes.marginTop16)} >
                <div className={classNames(classes.layoutValue, workingHour === "all"? classes.selected:"")} 
                    onClick={()=>{workingHour === "all" ? null : clickWorkingHour('all')}} >
                    <div className={classes.label}>8시 ~ 익일 8시</div>
                    <div className={classes.value}>전체</div>
                </div>
                <div className={classNames(classes.layoutValue, workingHour === "day"? classes.selected:"")} 
                    onClick={()=>{workingHour === "day" ? null : clickWorkingHour('day')}} style={{margin:'0px 32px'}}  >
                    <div className={classes.label}>8시 ~ 20일</div>
                    <div className={classes.value}>주간</div>
                </div>
                <div className={classNames(classes.layoutValue, workingHour === "night"? classes.selected:"")}  
                    onClick={()=>{workingHour === "night" ? null : clickWorkingHour('night')}}>
                    <div className={classes.label}>20시 ~ 익일 8시</div>
                    <div className={classes.value}>야간</div>
                </div>
            </div>
        </div>
    )
}


export default WorkingHour;