import React, {useEffect, useState} from "react";
import { useRouter } from "next/router";

import Text from "../elements/Text";
import classes from './inquiry-layout.module.scss';
import classNames from "classnames";
import { EquipmentTypeProps } from "../../../types/common-types";


// 크기별 설비 그룹
const EquipmentType = ({
    equipmentTypeData
}:EquipmentTypeProps) => {
    const router = useRouter();
    const [type, setType] = useState<string|undefined>(equipmentTypeData);

    useEffect(() => {
        setType(equipmentTypeData);
    },[equipmentTypeData])

    const clickType = (clickedType:string) => {
        setType(clickedType);

        router.push(
            {query:{
                ...router.query,
                equipmentType: clickedType,
            }},
            undefined,
            {shallow: true}
        )

        // setEquipType(clickedType);
    }

    return(
        <div className={classes.inquiryBox}>
            <Text title="크기별 설비 그룹" weight="bold" type="body1" />
            <div className={classNames(classes.body, classes.marginTop34)}>
                <div className={classNames(classes.hoverValue, type === "all" && classes.selectedValue)}
                    onClick={()=>{type === "all" || clickType('all')}}
                >전체 장비</div>
                <div 
                    className={classNames(classes.hoverValue, type === "large" && classes.selectedValue)} 
                    style={{margin:'0px 32px'}}
                    onClick={()=>{type === "large" || clickType('large')}} 
                >대형 장비</div>
                <div 
                    className={classNames(classes.hoverValue, type === "medium" && classes.selectedValue)} 
                    style={{margin:'0px 32px 0px 0px'}}
                    onClick={()=>{type === "medium" || clickType('medium')}}  
                >중형 장비</div>
                <div 
                    className={classNames(classes.hoverValue, type === "small" && classes.selectedValue)} 
                    onClick={()=>{type === "small" || clickType('small')}} 
                >소형 장비</div>
            </div>
        </div>
    )
}

export default EquipmentType;