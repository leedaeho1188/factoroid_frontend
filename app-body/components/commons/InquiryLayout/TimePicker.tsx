import React,{useEffect, useState} from "react";
import classNames from "classnames";
import { useRouter } from "next/router";

import classes from './inquiry-layout.module.scss';
import { MultipleCalendars } from "../Calendar/CalendarLayout";
import { TimePickerProps } from "../../../types/common-types";
import Text from "../elements/Text";


// 달력을 사용한 조회 기간 컴포넌트
const TimePicker = ({
    start = new Date(),
    end = new Date(),

}:TimePickerProps) => {
    const router = useRouter();
    // 달력이 있고 없고 조작하는 상태 값
    const [calendar, setCalendar] = useState<boolean>(false);
    // 달력이 보여줘야될 현재 날짜
    const [currentDate, setCurrentDate] = useState<Date>(new Date());
    // 조회 시작 일시 상태 값
    const [startDate, setStartDate] = useState<Date>(new Date(start.getFullYear(), start.getMonth(), start.getDate()));
    // 조회 종료 일시 상태 값
    const [endDate, setEndDate] = useState<Date>(new Date(end.getFullYear(), end.getMonth(), end.getDate()))

    console.debug('[TimePicker] renders');

    // DESCRIBE: props 변경될때마다 state값 업데이트
    useEffect(()=>{
        setStartDate(new Date(start.getFullYear(), start.getMonth(), start.getDate()));
        setEndDate(new Date(end.getFullYear(), end.getMonth(), end.getDate()));
    },[start, end])


    // DESCRIBE: 조회 시작 일시 클릭했을 때 
    const handleStartCalendar = () => {
        if(calendar){
            setCalendar(false)
        }else{
            setCurrentDate(startDate)
            setCalendar(true)
        }
    }
    // DESCRIBE: 조회 종료 일시 클릭했을 때
    const handleEndCalendar = () => {
        if(calendar){
            setCalendar(false);
        }else{
            setCurrentDate(endDate);
            setCalendar(true);
        }
    }

    // DESCRIBE: 날짜 적용하기 버튼 클릭 했을 때
    // TODO: 예외 처리
    const onClickSubmitBtn = (start:Date, end:Date) => {
        setStartDate(start);
        setEndDate(end);
        // console.log(router.query)
        router.push(
            {query:{
                ...router.query,
                startDate:start.toDateString(),
                endDate:end.toDateString(),
            }},
            undefined,
            {shallow:true}
        )

        setCalendar(false);
    }



    return(
        <div className={classes.inquiryBox} >
            <Text title="조회 기간" weight="bold" type="body1" />
            <div className={classNames(classes.body,classes.marginTop16)}>
                <div className={classes.layoutValue}
                    onClick={handleStartCalendar}
                >
                    <div className={classes.label} >조회 시작 일시</div>
                    <div className={classes.value} style={{color:'#116d38'}}>{startDate.getFullYear()}년 {startDate.getMonth()+1}월 {startDate.getDate()}일</div>
                </div>
                <div className={classes.middle} >~</div>
                <div className={classes.layoutValue}
                    onClick={handleEndCalendar}
                >
                    <div className={classes.label} >조회 종료 일시</div>
                    <div className={classes.value} style={{color:'#116d38'}}>{endDate.getFullYear()}년 {endDate.getMonth()+1}월 {endDate.getDate()}일</div>
                </div>
            </div>
            <div className={classNames(classes.calendar)} >
            {/* 삼항 연산자로 달력 렌더링 */}
            {calendar
                ? <MultipleCalendars onClickSubmitBtn={onClickSubmitBtn} date={currentDate} start={startDate} end={endDate} />
                : null
            }
            </div>
        </div>
    )
}

export default React.memo(TimePicker);