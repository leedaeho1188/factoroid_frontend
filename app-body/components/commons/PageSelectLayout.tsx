import Link from 'next/link';
import { customerPages } from '../../shared/customerInfo';
import { useRouter } from "next/router";
import React, {useEffect, useState} from 'react';
import classes from './common.module.scss'
import { pageInfo } from '../../types/common-types';


const PageList = () => {

    const [pages, setPages] = useState<pageInfo[]>([]);
    const [customer, setCustomer] = useState<any>('');
     const router = useRouter();

    useEffect(()=>{
        const currentCustomer:any = customerPages.find((element)=>{
            return window.location.href.includes(element.customer.path);
        })
        setPages(currentCustomer.pages);
        setCustomer(currentCustomer.customer);
    })

    const setPage=(customer:string, page:string)=>{
        const path =`/${customer}/${page}`;
        router.push(path);
    }
    return (
        <div className={classes.pageSelectLayout}>
            {pages.map((page, idx)=>{
                return(
                    <>
                    {/* // <Link key={idx} href={`/${customer.path}/${page.path}` } passHref> */}
                        <div className={classes.pageSelectButton} onClick={()=>setPage(customer.path, page.path)}>{page.title}</div>
                    {/* // </Link> */}
                    </>
                )
            })}
        </div >
    )

}


export default PageList;
