import React, { useState } from 'react';
import { InputProps } from '../../../types/common-types';
import { inputs } from '../../../shared/style';

/**
 * 
 * @param type input 태그 type
 * @param name input 태그 name
 * @param label input 태그 label
 * @param placeholder input 태그 placeholder
 * @param value input 태그 value
 * @param helperText 버튼 클릭 전 또는 클릭 후 결과에 따른 안내
 * @param focused focus 상태인지에 따라 Input box border, value = true || false
 * @param textInput , value = true || false
 * @param status , value = dafault || nice || danger
 * @param size input box 높이, value = small || medium
 * @param icon input 필드에 아이콘이 들어가는지, value = true || false
 * @param iconPaths icon 이미지 경로, focused: onFocus 상태일 때, active: status가 nice 또는 danger 일 때, default: status가 default일 때
 * @param width input 태그 넓이, 필요 시 px로 재할당
 * @param borderRadius input 태그 테두리 둥글기, 필요 시 px로 재할당
 * @param onChange Input value onChange 함수
 * @param onClick icon === true 일 때 icon onClick 함수
 * @returns 
 */

const Input = ({type, name, label, placeholder, value, helperText, textInput, status, size, icon, iconPaths, width="225px", borderRadius="4px", onChange, onClick}: InputProps) => {

    const [inputName, setInpueName] = useState(name? name : "")
    const [inputLabel, setInputLabel] = useState(label? label : "")

    const [focused, setFocused] = useState<boolean>(false);

    const handleOnFocus = (e: React.FocusEvent<HTMLInputElement>): void => {
        if (focused) return
        setFocused(true)
    }

    const handleOnBlur = (e: React.FocusEvent<HTMLInputElement>): void => {
        if (!focused) return
        setFocused(false)
    }

    return (
        <>
        <div style={{
            width: "100%",
        }}>
        <label htmlFor={inputName} style={{
                display: "block",
                height: inputs.size[size].height.label,
                color: focused ? inputs[status].focused.color.default : inputs[status].default.color.white,
                fontSize: inputs.size[size].fontSize.labelText,
                fontWeight: inputs.fontWeight.bold,
            }}>
                {inputLabel}
            </label>
            <div style={{
                position: "relative",
            }}
            >
                <input
                    type={type}
                    name={inputName}
                    value={value}
                    onFocus={handleOnFocus}
                    onBlur={handleOnBlur}
                    style={{
                            width: width,
                            height: inputs.size[size].height.input,
                            backgroundColor: focused ? inputs[status].focused.backgroundColor : inputs[status].default.backgroundColor,
                            borderRadius: borderRadius,
                            outline: focused ? inputs[status].focused.outline : inputs[status].default.outline,
                            fontSize: inputs.size[size].fontSize.mainText,
                            color: focused ? inputs[status].focused.color.gray : inputs[status].default.color.gray,
                            cursor: focused ? inputs[status].focused.cursor : inputs[status].default.cursor,
                            padding: inputs.size[size].padding,
                            margin: inputs.size[size].margin,
                        }}
                    onChange={onChange}
                    placeholder={focused ? "" : placeholder}
                />
                {icon &&
                    <img src={focused? iconPaths?.focused : status === "default" ? iconPaths?.default : iconPaths?.active} 
                    style={{
                        position: "absolute",
                        top: "50%",
                        transform: "translateY(-50%)",
                        right: "8px",
                        width: "24px",
                        height: "24px",
                        cursor: "pointer",
                    }} 
                    className={inputName}
                    onClick={onClick}
                    alt=""
                    />
                }
            </div>

            <p style={{
                height: inputs.size[size].height.helperText,
                fontSize: inputs.size[size].fontSize.helperText, 
                color: helperText ? inputs[status].default.color[status] : inputs[status].default.color.white,
                }}
            >{helperText ? helperText : ""}</p>
        </div>

        </>
    )
}

export default Input