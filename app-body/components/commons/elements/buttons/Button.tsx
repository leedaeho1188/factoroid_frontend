import React, { useEffect, useState } from "react";
import Image from "next/image";

import { ButtonProps } from "types/common-types";
import { buttons } from "shared/style";
import RotateIcon from "public/images/rotate-icon.png"
import classes from "./button.module.scss"

// $TODO: 버튼 모듈화에 필요한 props 받아오기

/**
 * 
 * @param text 버튼에 들어가는 text
 * @param size 버튼 사이즈, value = large || medium
 * @param color 버튼 컬러,  value = primary || secondary || gray || white
 * @param style 버튼 스타일, value= text || line || solid
 * @param onClick 클릭 이벤트 함수
 * @param width 버튼 width, defaultValue = "fit-content", 필요시 px값을 할당하면 됨
 * @param height 버튼 height, defaultValue = "fit-content", 필요시 px값을 할당하면 됨
 * @param borderRadius 버튼 border-radius, defaultValue = "4px", 필요시 px값으로 할당하면 됨
 * @param state 버튼 상태, defaultValue ="default", value = "default" || "focused" || "disabled"
 * @param isLoading 로딩 중일 때 버튼 text 대신 로딩바가 돌 수 있도록, value = true || false
 * @param icon 아이콘 유무, defaultValue = false, value = true || false
 * @param iconPath 아이콘 이미지 경로
 * @returns 
 */


const Button = ({
    text,
    size,
    style,
    state = "default",
    width="fit-content",
    height="fit-content",
    borderRadius= "4px",
    justifyContent="center",
    isLoading,
    icon,
    iconImg,
    iconWidth,
    iconHeight,
    onClick,
}:ButtonProps) => {

    // $TODO: is_focused에따라 상태값 변환 시켜주기 
    const [ btnState, setbtnState ] = useState(state);

    // $TODO: 마우스가 버튼위에 있을 때
    const onMouseEnter = () => {
        if(btnState === "default"){
            setbtnState("hover")
        }
    };
    // $TODO: 마우스가 버튼에서 벗어났을때
    const onMouseLeave = () => {
        if(btnState === "hover"){
            setbtnState("default");
        }
    }

    useEffect(() => {
        // disabled ? setbtnState("disable") : setbtnState("default");
        return;
    }, [btnState])

    return(
            <div 
                style={{
                    color: isLoading ? "transparent" : buttons[style][btnState].color,
                    backgroundColor: buttons[style][btnState].backgroundColor,
                    border: buttons[style][btnState].border,
                    textDecoration: buttons[style][btnState].textDecoration,
                    cursor: buttons[style][btnState].cursor,
                    padding: buttons[style].padding[size],
                    borderRadius: borderRadius,
                    width: width,
                    height: height,
                    fontSize : buttons.fontSize[size],
                    fontWeight : "bold",
                    // button width가 줄어들었을 때 text 줄바꿈 block
                    whiteSpace: "nowrap",
                    // textAlign: "center",
                    display: "flex",
                    justifyContent: justifyContent,
                    alignItems: "center",
                    position: "relative",
                    gap: "6.67px"
                }} 
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
                onClick={onClick}
            >
                {isLoading
                    ? <img className={classes.rotateIcon} src={RotateIcon.src} alt="rotate-icon" />
                    : null
                }
                {icon
                    ? <Image width={iconWidth} height={iconHeight}  src={iconImg} alt="button-icon" />
                    : null
                }

                {text}
            </div>
    )
}

export default Button