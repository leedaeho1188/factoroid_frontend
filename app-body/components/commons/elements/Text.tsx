import React from "react";
import { TitleProps } from "../../../types/common-types";
import { colors, fonts  } from "../../../shared/style";


/**
 * 
 * @param title title = 컴포넌트에 들어가는 text 
 * @param type value = header | body | caption
 * @param color value from color 모듈s
 * @param weight defaultValue = regular
 * @param cursor defaultValue = default
 * @returns 
 */


// global Title 컴포넌트
const Text = ({
        title,
        type = "body1",
        color = "darkHigh",
        weight = "regular",
        cursor = "default"
    }:TitleProps) => {

    return (
            <div style={{
                    fontSize:fonts[type],
                    color:colors[color],
                    fontWeight:weight,
                    cursor: cursor,
                }}
            >{title}</div >
    )
}

export default Text;



