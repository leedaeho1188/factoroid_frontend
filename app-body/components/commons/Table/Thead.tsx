import React from 'react'
import Th from './Th';

interface TheadProps{
    options:any[];
}

export default function Thead({options}:TheadProps) {

    console.log('thead 옵션', options);

    return (
      <thead>
        {options.map((x,i_)=>{
            return (
            <tr key={`tr-${i_}`}>
                {x.value.map((y:string,i:number)=>{
                    return (
                        <Th
                          rowSpan={x.rowSpan !== undefined ? x.rowSpan[i] : 1}
                          colSpan={x.colSpan !== undefined ? x.colSpan[i] : 1}
                          scope={x.scope !== undefined ? x.scope[i] : "col"}
                          key={`Th-${i}`}
                        >
                          {x.value !== undefined ? x.value[i] : 1}
                        </Th>
                    );
                })}
            </tr>
            )
        })}
      </thead>
    );
}
