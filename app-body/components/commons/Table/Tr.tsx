import React from 'react'
import Td from './Td';
import classes from "./table.module.scss";

interface TrProps{
    index: number;
    data: number[];
}

export default function Tr({index ,data} : TrProps) {
    return (
      <tr className={index%2==1? classes.tr1: classes.tr2}>
        {
            data.map((x, i)=>{
                return (<Td key={`Td-${i}`}>{x}</Td>)
            })
        }
      </tr>
    );
}
