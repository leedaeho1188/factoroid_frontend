import React, { ReactChild, ReactChildren } from "react";
import classes from "./table.module.scss";

interface TdProps {
  children?: ReactChild | ReactChild[] | ReactChildren | ReactChildren[];
}

export default function Td({ children }: TdProps) {
  return <td className={classes.td} style={{textAlign: 'center'}}>{children}</td>;
}
