import React from 'react'
import Tr from "./Tr";

interface TbodyProps{
    data:number[][];
}

export default function Tbody({data}:TbodyProps) {
    return (
        <tbody>
            {
                data.map((x,i)=>{
                    return(<Tr index={i} key={`Tr-${i}`} data={x}></Tr>)
                })
            }
        </tbody>
    )
}
