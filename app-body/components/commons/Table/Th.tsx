import React, {ReactChild, ReactChildren} from 'react'
import classes from "./table.module.scss";

interface ThProps {
  rowSpan?: number;
  colSpan?: number;
  scope?: string;
  children?: ReactChild | ReactChild[] | ReactChildren | ReactChildren[];
}

export default function Th({rowSpan, colSpan, scope, children}: ThProps) {
    return (
        <th rowSpan={rowSpan} colSpan={colSpan} scope={scope} className={classes.th}>{children}</th>
    )
}
