import React from 'react';
import Thead from './Thead';
import Tbody from './Tbody';
import classes from "./table.module.scss";

interface Header{
    rowSpan?: number[];
    colSpan?: number[];
    scope?: string[];
    value: string[];
}

interface TableProps{
    options:{
        header: Header[];
        data: number[][];
    }
}
/**
 * 
 * @param options 테이블 속성 
 * @param options.header List:테이블 헤더 옵션
 * @param options.header.rowSpan 해당 헤더 세로 셀 병합 
 * @param options.header.colSpan 해당 헤더 가로 셀 병합
 * @param options.header.scope 테이블 헤더 scope 속성
 * @param options.header.value 테이블 헤더 value
 * @param options.data 테이블 body 에 들어갈 값
 * @returns 
 */
export default function Table({options}:TableProps) {

    return (
      <div>
        <table className={classes.table}>
            <Thead options={options.header} key={"Thead"}></Thead>
            <Tbody data={options.data} key={"Tbody"}></Tbody>
        </table>
      </div>
    );
}
