import React from 'react';
import classNames from 'classnames';

import { MyPageModalProps } from 'types/common-types';
import classes from './myPage-modal.module.scss';
import Button from '../elements/buttons/Button';
import ButtonIcon from 'public/images/icon-reset-40x40.png'


const MyPageModal = ({
    userInfo, 
    setMyPageModal,
    setPasswordChangeModal
}:MyPageModalProps) => {

    console.log(userInfo);

    const clickChangePasswordBtn = () => {
        setMyPageModal(false);
        setPasswordChangeModal(true);
    }

    return (
        <div className={classes.modalContainer} >
            <div className={classes.modalOuterBox} ></div>
            <div className={classes.modalInnerBox} >
                <div className={classes.header} >
                    <div className={classNames(classes.body1, classes.bold)} >{userInfo.name}</div>
                    <div className={classes.body2}>{`(ID: ${userInfo.username})`}</div>
                    <Button
                        text="비밀번호 변경"
                        size="small"
                        style="line"
                        icon
                        iconImg={ButtonIcon}
                        iconWidth="14.67px"
                        iconHeight="14.67px"
                        onClick={clickChangePasswordBtn}
                    />
                </div>
                <div className={classes.body2}>{userInfo.isSuper? "관리자":"일반 사용자"}</div>
                <div className={classes.body2}>{userInfo.department}</div>
                <div className={classes.body2}>{userInfo.email}</div>
                <div className={classes.body2}>{userInfo.phone}</div>
                <Button
                    text="창 닫기"
                    size="large"
                    style="solid"
                    width="100%"
                    onClick={()=>{setMyPageModal(false)}}
                />
            </div>
        </div>
    )
}


export default MyPageModal