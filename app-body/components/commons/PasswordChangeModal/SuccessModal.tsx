import React from "react";
import classNames from "classnames";
import classes from './password-change-modal.module.scss';

import DoneIcon from "../../../public/images/icon-done-40x40.png";
import Reseticon from "../../../public/images/icon-reset-40x40.png"

// $TODO: 성공 모달 컴포넌트에 필요한 props

/**
 * 
 * @param type 아이콘 타입 , value = "done" || "reset"
 * @param text 아이콘 아래에 들어가는 성공 알림 내용의 text
 * @returns 
 */

interface SuccessModalProps {
    type: string;
    text: string;
}

const SuccessModal = ({type, text}: SuccessModalProps) => {
    return (
        <>
        <div className={classNames(classes.modalContainer, classes.successModal)} >
            <div>
                <img src={
                    type === "reset"
                    ? Reseticon.src
                    : DoneIcon.src} alt="" />
            </div>
            <p className={classes.mainText}>{text}</p>
        </div>
        </>
    )
}

export default SuccessModal