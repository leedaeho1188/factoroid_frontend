import React, { useState, useEffect } from "react";
import classes from './password-change-modal.module.scss';
import classNames from "classnames";
import { useDispatch, useSelector } from "react-redux"
import { CurrentLoginThunk, CurrentPasswordCheckThunk, ChangePasswordThunk, InitialPasswordCheck } from "../../../store/modules/login";
import { RootState } from "../../../store/modules";

import Input from "../elements/Input";
import Button from "../elements/buttons/Button";

import VisibilityIconDefault from "../../../public/images/icon-visibility-24x24-0.28.png";
import VisibilityIconActive from "../../../public/images/icon-visibility-24x24-0.64.png";
import VisibilityIconFocused from "../../../public/images/icon-visibility-24x24-0.87.png";
import AlertIconRed from "../../../public/images/icon-alert-red-20x20.png";
import AlertIconYellow from "../../../public/images/icon-alert-yellow-20x20.png";
interface PasswordsType {
    newPW: string;
    checkPW: string;
}

const VisibilityIcons = {
    default: VisibilityIconDefault.src, 
    active: VisibilityIconActive.src,
    focused: VisibilityIconFocused.src,
}

const PasswordChangeModal = ({setPasswordChangeModal, setSuccessModal, setFailureModal}: any) => {

    const dispatch = useDispatch();

    const [accountId, setAccoundId] = useState<null | number>(null);

    const [curPWInputType, setCurPWInputType] = useState("password");
    const [newPWInputType, setNewPWInputType] = useState("password");
    const [checkNewPWInputType, setCheckNewPWInputType] = useState("password");

    const [curPWVisible, setCurPWVisible] = useState<boolean>(false);
    const [newPWVisible, setNewPWVisible] = useState<boolean>(false);
    const [checkNewPWVisible, setCheckNewCurPWVisible] = useState<boolean>(false);

    const [curPWInput, setCurPWInput] = useState("");
    const [newPWInput, setNewPWInput] = useState("");
    const [checkNewPWInput, setCheckNewPWInput] = useState("");

    const [curPWStatus, setCurPWStatus] = useState({
        status: "default",
        helperText: "",
    });
    const [newPWStatus, setNewPWStatus] = useState({
        status: "default",
        helperText: "",
    });
    const [checkNewPWStatus, setCheckNewPWStatus] = useState({
        status: "default",
        helperText: "",
    });

    const [changeBtnDisabled, setChangeBtnDisabled] = useState(true);

    const storedStatus = useSelector((state: RootState) => state.login.status);

    const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target;
        switch (name) {
            case "curPassword": {
                setCurPWInput(value);
                if (curPWStatus.status === "danger") {
                    setCurPWStatus({
                        status: "default",
                        helperText: "",
                    })
                }
                break;
            }
            case "newPassword": {
                setNewPWInput(value);
                handleCheckPWValid(value);
                handleCheckPWSame({
                    newPW: value,
                    checkPW: checkNewPWInput
                });
                break;
            }
            case "checkNewPassword": {
                setCheckNewPWInput(value);
                handleCheckPWSame({
                    newPW: newPWInput,
                    checkPW: value
                });
                break;
            }
            default:
                break;
        }
    }

    const handleChangeButtonStatus = () => {
        if ( curPWInput.length > 0 && newPWStatus.status === "nice" && checkNewPWStatus.status === "nice") {
            setChangeBtnDisabled(false)
        } else {
            setChangeBtnDisabled(true)
        }
    }

    const handleCheckPWValid = (value: string) => {
        // DESCRIBE: 특수문자는 !@#$%^&*.()만 사용 가능합니다.
        const regex1 = /^[A-Za-z\d@$!%*#?&.()]{1,}$/
		// DESCRIBE: 최소 8자 이상을 입력해주세요.
		const regex2 = /^[A-Za-z\d@$!%*#?&.()]{8,}$/
		// DESCRIBE: 영문자, 숫자, 특수문자 3가지를 모두 포함해야합니다.
		const regex3 = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&.()])[A-Za-z\d@$!%*#?&.()]{8,}$/

        if (value.length === 0) {
            return setNewPWStatus({
                status: "default",
                helperText: "",
            })
        }
        else if (!regex1.test(value)) {
            return setNewPWStatus({
                status: "danger",
                helperText: "특수문자는 !@#$%^&*.()만 사용 가능합니다.",
            })
        }
        else if (!regex2.test(value)) {
            return setNewPWStatus({
                status: "danger",
                helperText: "최소 8자 이상을 입력해주세요.",
            })
        }
        else if (!regex3.test(value)) {
            return setNewPWStatus({
                status: "danger",
                helperText: "영문자, 숫자, 특수문자 3가지를 모두 포함해야합니다.",
            })
        }
        else if (curPWInput === value) {
            return setNewPWStatus({
                status: "danger",
                helperText: "현재 비밀번호와 다른 값을 입력해주세요.",
            })
        }
        else {
            return setNewPWStatus({
                status: "nice",
                helperText: "",
            })  
        }

    }

    const handleCheckPWSame = (passwords: PasswordsType) => {
        if (passwords.checkPW.length === 0) {
            return setCheckNewPWStatus({
                status: "default",
                helperText: "",
            });
        }
        else if (passwords.newPW !== passwords.checkPW) {
            return setCheckNewPWStatus({
                status: "danger",
                helperText: "비밀번호가 일치하지 않습니다.",
            });
        }
        else if (passwords.newPW === passwords.checkPW) {
            return setCheckNewPWStatus({
                status: "nice",
                helperText: "",
            });
        }
    }

    const handleClickIcon = (e: React.MouseEvent<HTMLDivElement>) => {
        e.preventDefault();
        e.stopPropagation();

        // DESCRIBE: 
        const { className } = e.currentTarget;
        if (className === "curPassword") {
            if (!curPWVisible) {
                setCurPWVisible(true);
                setCurPWInputType("text");
            } else {
                setCurPWVisible(false);
                setCurPWInputType("password");
            }
        }
        else if (className === "newPassword") {
            if (!newPWVisible) {
                setNewPWVisible(true);
                setNewPWInputType("text");
            } else {
                setNewPWVisible(false);
                setNewPWInputType("password");
            }
        }
        else if (className === "checkNewPassword") {
            if (!checkNewPWVisible) {
                setCheckNewCurPWVisible(true);
                setCheckNewPWInputType("text");
            } else {
                setCheckNewCurPWVisible(false);
                setCheckNewPWInputType("password");
            }
        }
    }

    const fetchCurrentLogin = async () => {

        try {
            const result = await dispatch(CurrentLoginThunk());

            const code = result?.payload.result.code;
            const id = result?.payload.result.data.id;
    
            if (code === 200) {
                setAccoundId(id)
            }
            else if (code === -111) {
                // DESCRIBE: 세션 만료
            }
        } catch (error) {
            console.log(error);
        }
    }

    const handleClickChangeBtn = async (e: React.MouseEvent<HTMLDivElement>) => {
        e.preventDefault();
        e.stopPropagation();

        if (!accountId || changeBtnDisabled) return;

        try {
            const result = await dispatch(CurrentPasswordCheckThunk({accountId: accountId, password: curPWInput}));
            const code = result.payload.result.code;
    
            handleCheckCurrentPassword(accountId, code);
        } catch (error) {
            setPasswordChangeModal(false);
            setFailureModal(true);
        }
    }

    const handleCheckCurrentPassword = (accountId: number, code: number) => {

        // DESCIRBE: 200 => 비밀번호 일치
        if (code === 200) {
            // DESCRIBE: 비밀번호 변경 api 호출 함수
            handleChangePassword(accountId)
        }
        // DESCRIBE: -102 => 비밀번호 불일치
        else if (code === -102) {
            setCurPWStatus({
                status: "danger",
                helperText: "잘못된 비밀번호입니다. 비밀번호를 다시 입력해주세요."
            });
        }
        // DESCRIBE: -109 => 해당 account가 없음
        else if (code === -109) {
        }
        else {
            setPasswordChangeModal(false);
            setFailureModal(true);
        }
    }

    const handleChangePassword = async (accountId: number) => {
        if (!accountId) return;

        const result = await dispatch(ChangePasswordThunk({accountId: accountId, password: newPWInput}))
        const code = result.payload.result.code;
        console.log(result);
        console.log(code);

        // DESCRIBE: 200 => 비밀번호 변경 성공
        if (code === 200) {
            console.log("비밀번호 변경 완료!");
            setPasswordChangeModal(false);
            setSuccessModal(true);
            setTimeout(() => {
                setSuccessModal(false);
                handleInitState();
            }, 1300)
        }
        // DESCRIBE: -104 => 새로운 비밀번호 유효성 검사 실패
        else if (code === -104) {
        }
        // DESCRIBE: -109 => 해당 account가 없음
        else if (code === -109) {
        }
        // DESCRIBE: 예측할 수 없는 원인으로 통신 실패
        else {
            setPasswordChangeModal(false);
            setFailureModal(true);
        }
    }

    const handleInitState = () => {
        // DESCRIBE: 모든 input value 및 상태 초기화
        dispatch(InitialPasswordCheck(false));
        setPasswordChangeModal(false);

        setAccoundId(null);

        setCurPWInputType("password");
        setNewPWInputType("password");
        setCheckNewPWInputType("password");

        setCurPWInput("");
        setNewPWInput("");
        setCheckNewPWInput("");

        setCurPWVisible(false);
        setNewPWVisible(false);
        setCheckNewCurPWVisible(false);

        setCurPWStatus({
            status: "default",
            helperText: "",
        })
        setNewPWStatus({
            status: "default",
            helperText: "",
        })
        setCheckNewPWStatus({
            status: "default",
            helperText: "",
        })

    }

    useEffect(() => {
        // DESCRIBE: fetch/current-login, 현재 로그인한 유저의 accountId 획득
        fetchCurrentLogin();
    }, [])

    useEffect(() => {
        handleChangeButtonStatus();
    }, [curPWInput, newPWStatus.status, checkNewPWStatus.status])

    return (
        <>
        <div className={classNames(classes.modalContainer, classes.PWResetModal)}>
            <Input
                type={curPWInputType}
                name="curPassword"
                placeholder="현재 비밀번호"
                label="현재 비밀번호"
                value={curPWInput}
                status={curPWStatus.status}
                helperText={curPWStatus.helperText}
                size="medium"
                width="100%"
                onChange={handleOnChange}
                icon={true}
                iconPaths={VisibilityIcons}
                onClick={handleClickIcon}
            />
            <div className={classes.information}>
                <p className={classes.mainText}>
                Factoroid의 안전한 계정 사용을 위해
                <br/>
                아래 가이드를 따라 비밀번호를 설정해주세요.
                </p>
                <div className={classNames(classes.flex, classes.flexColumn)}>
                    <div className={classNames(classes.flex, classes.flexRow)}>
                        <div  className={classes.iconWrapper}>
                            <img src={AlertIconRed.src} alt="alert-icon-red" />
                        </div>
                        <p className={classes.mainText}>필수 조건</p>
                    </div>
                    <p className={classes.subText}>
                    - 최소 8자 이상이어야 합니다.
                    <br/>
                    - 영문자, 숫자, 특수문자 3가지를 모두 포함해야 합니다.
                    <br/>
                    - 특수문자는 !@#$%^&*.()만 사용 가능합니다.
                    </p>
                </div>
                <div className={classNames(classes.flex, classes.flexColumn)}>
                    <div className={classNames(classes.flex, classes.flexRow)}>
                        <div className={classes.iconWrapper}>
                            <img src={AlertIconYellow.src} alt="alert-icon-yellow" />
                        </div>
                        <p className={classes.mainText}>권장 사항</p>
                    </div>
                    <p className={classes.subText}>
                    - 동일한 문자의 반복은 피해주세요.
                    <br/>
                    - 키보드 상에서 나란히 있는 문자열은 피해주세요.
                    </p>
                </div>
            </div>
            <Input
                type={newPWInputType}
                name="newPassword"
                placeholder="새 비밀번호"
                label="새 비밀번호"
                value={newPWInput}
                status={newPWStatus.status}
                helperText={newPWStatus.helperText}
                size="medium"
                width="100%"
                onChange={handleOnChange}
                icon={true}
                iconPaths={VisibilityIcons}
                onClick={handleClickIcon}
            />
            <Input
                type={checkNewPWInputType}
                name="checkNewPassword"
                placeholder="새 비밀번호 재입력"
                label="새 비밀번호 재입력"
                value={checkNewPWInput}
                status={checkNewPWStatus.status}
                helperText={checkNewPWStatus.helperText}
                size="medium"
                width="100%"
                onChange={handleOnChange}
                icon={true}
                iconPaths={VisibilityIcons}
                onClick={handleClickIcon}
            />
            <div className={classes.btnsWrapper}>
                <Button
                    text="취소하기"
                    size="large"
                    color="gray"
                    style="text"
                    onClick={handleInitState}
                />
                <Button
                    text="비밀번호 변경 완료"
                    size="large" color="primary"
                    style="solid"
                    width="173.03px"
                    height="40px"
                    disabled={changeBtnDisabled}
                    isLoading={storedStatus === "pw-loading" ? true : false}
                    onClick={handleClickChangeBtn}
                />
            </div>
          </div>
        </>
    )
}

export default PasswordChangeModal