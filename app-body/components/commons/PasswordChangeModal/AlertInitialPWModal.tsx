import React, { useState } from "react";
import classes from './password-change-modal.module.scss';
import classNames from "classnames";
import Image from "next/image";
import { useDispatch } from "react-redux"
import { InitialPasswordCheck } from "../../../store/modules/login";

import Button from "../elements/buttons/Button";
import AlertIconRed from "../../../public/images/icon-alert-red-40x40.png"

const AlertInitialPWModal = ({setAlertInitialPWModal, setPasswordChangeModal}: any) => {

  const dispatch = useDispatch();

  const handleClickLaterBtn = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    e.stopPropagation();

    dispatch(InitialPasswordCheck(false));
    setAlertInitialPWModal(false);
  }

  const handleClickNowBtn = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    e.stopPropagation();

    setPasswordChangeModal(true);
    setAlertInitialPWModal(false);
  }

  return (
    <>
      <div className={classNames(classes.modalContainer, classes.afterLoginModal)}>
        <div>
          <img src={AlertIconRed.src} alt="" />
        </div>
        <p className={classes.mainText}>계정 보안을 위해 비밀번호 변경이 필요합니다.</p>
        <p className={classes.subText}>고객님은 현재 초기 비밀번호를 사용중입니다.<br/>안전한 계정 사용을 위해 비밀번호를 변경해주세요.</p>
        <div className={classes.btnsWrapper}>
          <Button text="나중에" size="large"  color="gray" style="text" onClick={handleClickLaterBtn} />
          <Button text="지금 비밀번호 변경하기" size="large" color="primary" style="solid" onClick={handleClickNowBtn} />
        </div>
      </div>
    </>
  )
}

export default AlertInitialPWModal