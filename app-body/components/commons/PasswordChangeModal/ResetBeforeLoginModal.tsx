
import React, { useState, useEffect } from 'react'
import classes from './password-change-modal.module.scss';
import classNames from "classnames";

import Input from '../elements/Input'
import Button from "../elements/buttons/Button"


const ResetBeforeLoginModal = ({setResetBeforeLoginModal}: any) => {

    const [buttonStatus, setButtonStatus] = useState("gray");
    const [usernameStatus, setUsernameStatus] = useState("default")
    const [usernameInput, setUsernameInput] = useState("")

    const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = e.target;
        
        setUsernameInput(value);
    }

    useEffect(() => {
        usernameInput.length > 0
        ? setButtonStatus("primary")
        : setButtonStatus("gray")
    }, [usernameInput])

    const handleClickClose = () => {
        setResetBeforeLoginModal(false)
    }

    return (
        <>
            <div className={classes.modalOverlay}>
                <div className={classNames(classes.modalContainer, classes.beforeLoginModal)}>
                    <p className={classes.mainText}>등록된 ID 을 입력하시면 비밀번호 재설정을 위한<br/>인증번호를 이메일로 발송해드립니다.</p>
                    <Input type="text" name="username" placeholder="ID" label="ID" value={usernameInput} size="medium" status={usernameStatus} width="100%" onChange={handleOnChange}/>
                    <div className={classes.btnsWrapper}>
                        <Button text="창 닫기" size="large" color="gray" style="text" onClick={handleClickClose}/>
                        <Button text="인증번호 받기" size="large" color={buttonStatus} style="solid" />
                    </div>
              </div>
            </div>
        </>
    )
}

export default ResetBeforeLoginModal