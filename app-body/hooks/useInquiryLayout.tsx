import { useState, useRef, useEffect } from "react";
import { useRouter } from "next/router";
import { DateType } from "../types/common-types";

function useInquiryLayout(): [
  equipType:string,
  date: DateType,
  workingHour: string,
  setDate: React.Dispatch<React.SetStateAction<DateType>>,
  setWorkingHour: React.Dispatch<React.SetStateAction<string>>
] {

  const router = useRouter();
  // DESCRIBE: 설비 타입
  const [equipType, setEquipType] = useState<string>("all");
  // DESCRIBE: 근무 시간
  const [workingHour, setWorkingHour] = useState<string>("all");
  // DESCRIBE : 조회 기간
  const [date, setDate] = useState<DateType>({
    startDate: new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate() - 1
    ),
    endDate: new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate() - 1
    ),
  });

  // DESCRIBE: 첫번째 useEffect 방지를 위한 useRef
  const firstUpdate = useRef(true);
  const firstUpdate2 = useRef(true);

  //

  // DESCRIBE: 날짜 바뀌면(쿼리 스트링 바뀌면), 데이터 재요청 및 상태 업데이트
  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      return;
    }
    const date2 = {
      startDate: router.query.startDate
        ? new Date(router.query.startDate as string)
        : new Date(
            new Date().getFullYear(),
            new Date().getMonth(),
            new Date().getDate() - 1
          ),
      endDate: router.query.endDate
        ? new Date(router.query.endDate as string)
        : new Date(
            new Date().getFullYear(),
            new Date().getMonth(),
            new Date().getDate() - 1
          ),
    };

    const workingHour = router.query.workingHour
      ? (router.query.workingHour as string)
      : "all";

    setDate(date2);
    setWorkingHour(workingHour);
  }, [router.query.workingHour, router.query.startDate]);

  
  // DESCRIBE: 설비 크기가 변했을 때 안에 함수 작동
  useEffect(() => {
    // 컴포넌트 첫 렌더링 때 useEffect안에 함수들이 작동하지 않게 하기
    if (firstUpdate2.current) {
      firstUpdate2.current = false;
      return;
    }

    const equipmentType = router.query.equipmentType
      ? (router.query.equipmentType as string)
      : "all";

    setEquipType(equipmentType);
  }, [router.query.equipmentType]);


  return [equipType, date, workingHour, setDate, setWorkingHour];
}

export default useInquiryLayout;
