export async function getStaticProps(){
    const filePath= path.join(process.cwd(), 'data', 'dummy-backend.json');
    const jsonData = await fs.readFile(filePath);
    const data = JSON.parse(jsonData);

    return {
        props:{
            products: data.products
        },
        revalidate:10
    }
}